import { Forge } from "./types/forge.type";

export function forgeFactory (data: any): Forge {
    var { _id, userID, name, description, sourceLanguage, targetLanguage, imageURL } = data;
    return {
        _id,
        userID,
        name,
        description,
        sourceLanguage,
        targetLanguage,
        imageURL
    };
}