import { ObjectID } from "mongodb";
import { Language } from "../../languages/types/language.type";

export type Forge = {
    _id: ObjectID;
    userID: ObjectID;
    name: string;
    description: string;
    imageURL: string;
    sourceLanguage: Language;
    targetLanguage: Language;
};
