import { Document } from "mongoose";
import { ObjectID } from "mongodb";

export type ForgeDocument = {
    userID: ObjectID;
    name: string;
    description: string;
    imageURL: string;
    sourceLanguageID: ObjectID;
    targetLanguageID: ObjectID;
} & Document;