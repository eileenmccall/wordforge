import { Forge } from "./schemas/forge.schema";
import { ObjectID } from "mongodb";
import Language from "../languages/schemas/language.schema";
import { forgeFactory } from "./forge.factory";
import { LanguageDocument } from "../languages/types/languageDocument.type";
import { ForgeDocument } from "./types/forgeDocument.type";

export async function getUserForges (userID: string) {
    var forgeDocuments: Array<ForgeDocument> = await Forge.find({
        userID
    }) as any;

    var forges = await Promise.all(forgeDocuments.map(async function getForgeLanguages ( { _doc }: any) {
        var sourceLanguage = await Language.findById(_doc.sourceLanguageID);
        var targetLanguage = await Language.findById(_doc.targetLanguageID);

        var forge = forgeFactory({
            ..._doc,
            sourceLanguage,
            targetLanguage
        });
        return forge;
    }));

    return forges;
}

export async function addUserForge (userID: ObjectID, {
    sourceLanguage,
    targetLanguage,
    name,
    description,
    imageURL
}) {

    var source: LanguageDocument;
    var target: LanguageDocument;

    var { _id: sourceLanguageID } = source = await Language.findOne({ code: sourceLanguage });
    var { _id: targetLanguageID } = target = await Language.findOne({ code: targetLanguage });

    var forgeDocument = new Forge({
        userID,
        name,
        description,
        sourceLanguageID,
        targetLanguageID,
        imageURL
    });

    var { _doc } = await forgeDocument.save() as any;

    return forgeFactory({
        ..._doc,
        sourceLanguage: source,
        targetLanguage: target
    });
}

export async function deleteUserForge (forgeID) {
    var forge = await Forge.findById(forgeID);

    await forge.remove();

    return forge._id;
}