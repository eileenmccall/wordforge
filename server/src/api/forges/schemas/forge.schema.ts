import mongoose, { Schema } from "mongoose";
import { ObjectID } from "mongodb";

var forgeSchema = new Schema({
    userID: {
        type: ObjectID,
        required: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String,
        required: true,
        trim: true
    },
    imageURL: {
        type: String,
        required: false
    },
    sourceLanguageID: {
        type: ObjectID,
        required: true
    },
    targetLanguageID: {
        type: ObjectID,
        required: true
    }
});

export var Forge = mongoose.model("Forge", forgeSchema, "Forges");