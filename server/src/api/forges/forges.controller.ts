import { Router, Request, Response } from "express";
import { getUserForges, addUserForge, deleteUserForge } from "./forges.repository";
import { requireAuth } from "../auth/middleware/auth.middleware";

export default function forgesController (app: Router) {
    var route = Router();

    app.use("/forges", route);

    route.get("/", requireAuth, async function getForgesHandler (req: Request, res: Response) {
        var { userId } = req.signedCookies;

        var forges = await getUserForges(userId);

        res.status(200).json(forges);
    });

    route.post("/", requireAuth, async function addForgeHandler (req: Request, res: Response) {
        var { userId } = req.signedCookies;

        var forge = await addUserForge(userId, req.body);

        res.status(200).json(forge);
    });

    route.delete("/:forgeID", requireAuth, function deleteForgeHandler (req: Request, res: Response) {
        var { forgeID } = req.params;

        var deletedForgeID = deleteUserForge(forgeID);

        res.status(200).json(deletedForgeID);
    });
}