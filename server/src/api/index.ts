import { Router } from "express";
import mountAuthRoutes from "./auth/auth.controller";
import mountUsersRoutes from "./users/users.controller";
import mountWordsRoutes from "./words/words.controller";
import mountPartsOfSpeechRoutes from "./partsOfSpeech/parts-of-speech.controller";
import mountSessionsRoutes from "./sessions/sessions.controller";
import mountExercisesRoutes from "./exercises/exercises.controller";
import mountForgesRoutes from "./forges/forges.controller";
import mountLanguagesRoutes from "./languages/languages.controller";

export default function mountRoutes (): Router {
    var app = Router();

    mountAuthRoutes(app);
    mountUsersRoutes(app);
    mountForgesRoutes(app);
    mountWordsRoutes(app);
    mountPartsOfSpeechRoutes(app);
    mountLanguagesRoutes(app);
    mountSessionsRoutes(app);
    mountExercisesRoutes(app);

    app.use(function (req, res) {
        res.status(404).json({ message: "Not found!!" });
    });

    return app;
}