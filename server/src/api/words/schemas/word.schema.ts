import mongoose, { Schema } from "mongoose";
import WordDocument from "../interfaces/word-document.interface";
import { ObjectID } from "mongodb";
var { ObjectId } = Schema.Types;

var WordSchema = new Schema({
    root: {
        type: String,
        required: true,
        trim: true,
        lowercase: true
    },
    forgeID: {
        type: ObjectID,
        required: true
    },
    partOfSpeechId: {
        type: ObjectId,
        required: true,
        trim: true,
        lowercase: true
    },
    formIds: [
        {
            type: ObjectId,
            required: false,
            default: []
        }
    ],
    sentenceIds: [
        {
            type: ObjectId,
            required: false,
            default: []
        }
    ],
    exerciseIds: [
        {
            type: ObjectId,
            required: false,
            default: []
        }
    ],
    noteIds: [
        {
            type: ObjectId,
            required: false,
            default: []
        }
    ]
}, {
    collection: "Words"
});

var word = mongoose.model<WordDocument>("Word", WordSchema, "Words");
export default word;