import { ObjectId } from "mongodb";
import PartOfSpeech from "../../partsOfSpeech/interfaces/part-of-speech.interface";

export interface Word {
    _id: ObjectId;
    root: string;
    translation: string;
    partOfSpeech: PartOfSpeech;
    numberOfSentences: number;
    numberOfExercises: number;
    reviewPercentage: number;
}