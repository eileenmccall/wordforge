import { Request } from "express";

export type AddWordRequest = {
    params: {
        forgeID: string;
    };

    body: {
        root: string;
        translation: string;
        partOfSpeechID: string;
    }
} & Request;