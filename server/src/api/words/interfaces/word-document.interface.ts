import { ObjectId } from "mongodb";
import { Document } from "mongoose";

export default interface WordDocument extends Document {
    _id: ObjectId;
    root: string;
    partOfSpeechId: ObjectId;
    formIds: Array<ObjectId>;
    sentenceIds: Array<ObjectId>;
    exerciseIds: Array<ObjectId>;
    noteIds: Array<ObjectId>;
}