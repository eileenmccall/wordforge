import mongoose, { Schema } from "mongoose";
import { ObjectID } from "mongodb";
import { ExerciseDocument } from "../types/exercise.types";

var ExerciseSchema = new Schema({
    userId: {
        type: ObjectID,
        required: true
    },
    forgeId: {
        type: ObjectID,
        required: true
    },
    wordId: {
        type: ObjectID,
        required: true
    },
    createdDate: {
        type: Date,
        required: true
    },
    lastModified: {
        type: Date,
        required: false
    },
    lastReview: {
        type: Date,
        required: true,
        default: null
    },
    currentEase: {
        type: Number,
        required: true,
        default: 2.5
    },
    nextReview: {
        type: Date,
        required: true,
        default: null
    },
    exerciseIsNew: {
        type: Boolean,
        required: true,
        default: true
    },
    exerciseIsFlagged: {
        type: Boolean,
        required: true,
        default: false
    },
    exerciseIsLeech: {
        type: Boolean,
        required: true,
        default: false
    },
    sourceLanguage: {
        type: String,
        required: true,
        uppercase: true,
        trim: true
    },
    targetLangauge: {
        type: String,
        required: true,
        uppercase: true,
        trim: true
    },
    exerciseType: {
        type: String,
        required: true,
        trim: true
    },
    exercise: {
        type: Schema.Types.Mixed,
        required: true
    }
}, {
    collection: "Exercises"
});

export default mongoose.model<ExerciseDocument>("Exercise", ExerciseSchema);