import { ObjectId, ObjectID } from "mongodb";
import { Note } from "../../sessions/interfaces/note.types";
import { MediaURL } from "../../sessions/interfaces/media.types";
import { Document } from "mongoose";

export interface ExerciseDocument extends Document{
    userId: ObjectID;
    forgeId: ObjectID;

    isNew: boolean;
    isFlagged: boolean;
    isLeech: boolean;

    type: ExerciseTypeName;
    exercise: ExerciseType;

    sourceLanguage: string;
    targetLanguage: string;
}

export type Exercise = {
    _id: ObjectId;
    wordId: ObjectID;
    forgeId: ObjectID;

    isNew: boolean;
    isFlagged: boolean;
    isLeech: boolean;

    type: ExerciseTypeName;
    exercise: ExerciseType;

    sourceLanguage: string;
    targetLanguage: string;

    notes?: Array<Note>;
}

export type ExerciseTypeName =
    | "translateToTarget"
    | "translateToSource"
    | "pictureToWord"
    | "wordToPicture"
    | "fillInTheBlank"
    | "transcribeAudio"
    | "compareRecording"
    | "wordsToSentence";

export type ExerciseType =
    | TranslateToTargetExercise
    | TranslateToSourceExercise;

export type TranslateToTargetExercise = {
    source: string;
    acceptableAnswers: Array<string>;
    imageURLs: Array<MediaURL>;
    audioURLs: Array<MediaURL>;
};

export type TranslateToSourceExercise = {
    target: string;
    acceptableAnswers: Array<string>;
    imageURLs: Array<MediaURL>;
    audioURLs: Array<MediaURL>;
};

