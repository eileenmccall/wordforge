import { Router, Request, Response } from "express";
import * as exerciseRepository from "./exercises.repository";

export default async function exercisesController (app: Router) {
    var route = Router();

    app.use("/exercises", route);

    // route.get("", async function returnWordsList (req: Request, res: Response) {
    //     var words = await wordRepository.getWords();
    //     return res.json(words).status(200);
    // });

    route.get("/:wordId", async function getWordExercises (req: Request, res: Response) {

    });

    route.post("/:wordId", async function addExercises (req: Request, res: Response) {
        var { exercises } = req.body;
        var { wordId } = req.params;

        var docs = await exerciseRepository.addExercises(wordId, exercises);

        res.json(docs).status(200);
    });

    // route.post("", async function createWord (req: {} ) {

    // })

    return null;
}

