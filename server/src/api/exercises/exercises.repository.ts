import exerciseSchema from "./schemas/exercise.schema";
import Word from "../words/schemas/word.schema";

export async function addExercises (wordId, exercises = []) {
    exercises = exercises.map(function addDate (exercise) {
        return {
            ...exercise,
            wordId,
            createdDate: new Date()
        };
    });

    var exerciseDocuments;

    await exerciseSchema.create(exercises, function (err, documents) {
        exerciseDocuments = documents;
    });

    var word = await Word.findById(wordId);

    exerciseDocuments.forEach(function updateWordIds (document) {
        word.exerciseIds.push(document._id);
    });

    word.save();

    return exerciseDocuments;
}
