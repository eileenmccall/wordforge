import { Schema } from "mongoose";
import { ObjectID } from "mongodb";

export var answerSchema = new Schema({
    type: {
        answer: {
            type: String,
            required: true,
            lowercase: true,
            trim: true
        },
        correct: {
            type: Boolean,
            required: true
        },
        exerciseId: {
            type: ObjectID,
            required: true
        }
    }
});