import mongoose, { Schema } from "mongoose";
import { ObjectID } from "mongodb";
import { SessionDocument } from "../interfaces/session.interface";
import { answerSchema } from "../schemas/answer.schema";

var SessionSchema = new Schema({
    userId: {
        type: ObjectID,
        required: true
    },
    forgeId: {
        type: ObjectID,
        required: true
    },
    completed: {
        type: Boolean,
        required: true,
        default: false
    },
    expiresOn: {
        type: Date,
        required: true,
        default: new Date(Date.now() + 3600000)
    },
    duration: {
        type: String,
        required: false,
        trim: true,
        lowercase: true
    },
    strategy: {
        type: String,
        required: true,
        trim: true,
        lowercase: true
    },
    count: {
        type: String,
        required: false,
        trim: true,
        lowercase: true
    },
    currentExerciseIndex: {
        type: Number,
        required: true
    },
    exerciseIds: [
        {
            type: ObjectID,
            required: true,
            trim: true,
            lowercase: true
        }
    ],
    answers: [ answerSchema ],
}, {
    collection: "Sessions"
});

export default mongoose.model<SessionDocument>("Session", SessionSchema, "Sessions");
