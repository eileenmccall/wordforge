import { Router, Request, Response } from "express";
import { getNewSession } from "./sessions.repository";

export default async function sessionsController (app: Router) {
    var route = Router();

    /*
        Get user's sessions
        answer an exercise
        mark an exercise difficulty
        flag an exercise
        get the results of a session
    */

    app.use("/sessions", route);

    // start a new session
    route.post("", async function getNewSessionHandler (req: Request, res: Response): Promise<Response> {
        var { userId } = req.query;
        var sessionConfig = req.body;

        var session = await getNewSession(userId, sessionConfig);

        return res.json(session).status(200);

        // var session = {
        //     _id: new ObjectID(),
        //     totalExercises: exercises.length,
        //     exercise: exercises[0],
        //     completed: true
        // };

        // return res.json(session).status(200);
    });

    // route.post(
    //     ":sessionId/exercises/:exerciseId/answer",
    //     function answerExercise (req: Request, res: Response) {
    //         var { sessionId, exerciseId } = req.params;
    //         /*
    //             determine if user answer is correct
    //                 get acceptable answers for exercise
    //                 compare user answer with acceptable answers
    //             return answer object
    //         */


    //     }
    // );

    // // get the results of a session
    // route.post(":sessionId/results");

    return null;
}


