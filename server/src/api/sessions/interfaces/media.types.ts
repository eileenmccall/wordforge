import { ObjectID } from "mongodb";

export type MediaURL = {
    _id: ObjectID;
    type: "image" | "audio";
    name: string;
    url: string;
    wordIds?: Array<ObjectID>;
}