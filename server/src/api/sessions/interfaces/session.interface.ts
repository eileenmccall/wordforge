import { ObjectId, ObjectID } from "mongodb";
import { Exercise } from "../../exercises/types/exercise.types";
import { Document } from "mongoose";
import { Answer } from "./answer.interface";

export interface Session {
    userId: ObjectId;
    forgeId: ObjectId;

    startDate: Date;

    currentExercise: Exercise;
    totalExercises: number;
    currentExerciseIndex: number;
}

export interface SessionDocument extends Document {
    userId: ObjectId;
    forgeId: ObjectId;

    createdOn: Date;
    modifiedOn: Date;
    expiresOn: Date;

    completed: boolean;

    exerciseIds: Array<ObjectID>;
    currentExerciseId: ObjectID;

    answers: Array<Answer>;

    startDate: Date;
    endDate: Date;


}