export enum ReviewStrategy {
    "mostOverdue",
    "newOnly"
}

export interface SessionConfig {
    strategy: ReviewStrategy;
    duration: number;
    count: number | "infinite";
}