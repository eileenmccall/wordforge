import { ObjectId } from "mongodb";

export enum DifficultyRating {
    "veryDifficult",
    "difficult",
    "normal",
    "easy"
}

export interface Answer {
    exerciseId?: ObjectId;
    answer?: string;
    correct?: boolean;
    difficultyRating: DifficultyRating;
}
