import { ObjectId } from 'mongodb';

export type Note = {
    _id: ObjectId;
    body: string;
}
