import Session from "./schemas/session.schema";
import { ObjectID } from "mongodb";
import { SessionConfig } from "./interfaces/session-config.interface";
import Exercise from "../exercises/schemas/exercise.schema";
import { ExerciseDocument } from "../exercises/types/exercise.types";

export async function getNewSession (userId, sessionConfig: SessionConfig) {
    var { duration, strategy, count = 5 } = sessionConfig;

    var query: any = {
        userId
    };

    // if (strategy == ReviewStrategy.newOnly) {
    //     query.isNew = true;
    // }

    // if (strategy == ReviewStrategy.mostOverdue) {
    //     query.sort = { lastReview: "asc" };
    // }

    // if (count !== "infinite") {
    //     query.limit = count;
    // }

    var exerciseIds;
    Exercise.find(query, function (err, documents: Array<ExerciseDocument>) {
        exerciseIds = documents.map(function (x) {
            return x._id;
        });
    });

    var sessionDoc = new Session({
        userId: new ObjectID(),
        duration,
        strategy,
        count,
        exerciseIds
    });

    var session = await sessionDoc.save();

    return session;
}
