import { Session } from "./interfaces/session.interface";

export function sessionFactory ({
    userId,
    forgeId,
    exercises,
    startDate,
    currentExerciseIndex = 0
}): Session {
    var newSession: Session = {
        userId,
        forgeId,

        startDate,
        totalExercises: exercises.length,
        currentExerciseIndex,
        currentExercise: exercises[currentExerciseIndex],
    };
    return newSession;
}