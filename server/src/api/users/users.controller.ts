import { Router, Request, Response } from "express";
import { User } from "../auth/interfaces/user.interface";
import * as usersRepository from "./users.repository";

export default async function UsersController (app: Router) {
    var route = Router();

    app.use("/users", route);

    route.post("/activeForge/:forgeID", async function switchActiveForgeHandler (req: Request, res: Response) {
        var { userId } = req.signedCookies;
        var { forgeID } = req.params;

        try {
            var user: User = await usersRepository.setUserActiveForge(userId, forgeID);
            res.status(200).json(user);
        } catch (error) {
            res.status(500).json({ message: error });
        }
    });
}