import { ObjectID } from "mongodb";
import { UserModel } from "../auth/schemas/user.schema";
import { userFactory } from "./user.factory";
import { UserDocument } from "../auth/interfaces/user-document.interface";
import { User } from "../auth/interfaces/user.interface";

export async function getUserById (userId: ObjectID) {
    var user = await UserModel.findById(userId);
    return userFactory(user);
}

export async function setUserActiveForge (userID, forgeID): Promise<User> {
    try {
        var user: UserDocument = await UserModel.findById(userID);
    } catch {
        throw new Error("User not found!");
    }

    user.activeForgeID = forgeID;

    try {
        await user.save();
    } catch {
        throw new Error("Unable to save user");
    }

    return userFactory(user);
}