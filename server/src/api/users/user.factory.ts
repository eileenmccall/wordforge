import { User } from "../auth/interfaces/user.interface";
import { UserDocument } from "../auth/interfaces/user-document.interface";

export function userFactory ({
    _id,
    firstName,
    lastName,
    email,
    pronouns,
    activeForgeID
}: UserDocument): User {
    return {
        _id,
        firstName,
        lastName,
        get fullName (): string {
            return `${firstName} ${lastName}`;
        },
        email,
        pronouns,
        activeForgeID
    };
}