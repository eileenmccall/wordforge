import { Router, Request, Response } from "express";
import * as partsOfSpeechRepository from "./parts-of-speech.repository";

export default async function partsOfSpeechController (app: Router) {
    var route = Router();

    app.use("/partsOfSpeech", route);

    route.get("", async function returnPartsOfSpeech (req: Request, res: Response) {
        var partsOfSpeech = await partsOfSpeechRepository.getPartsOfSpeech();
        return res.json(partsOfSpeech).status(200);
    });

    return null;
}