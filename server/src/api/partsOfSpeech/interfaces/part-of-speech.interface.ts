import { ObjectId } from "mongodb";

export default interface PartOfSpeech {
    _id: ObjectId;
    name: string;
}
