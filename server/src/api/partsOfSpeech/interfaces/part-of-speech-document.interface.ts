import { Document } from "mongoose";
import { ObjectId } from "mongodb";

export default interface PartOfSpeechDocument extends Document {
    _id: ObjectId;
    name: string;
}
