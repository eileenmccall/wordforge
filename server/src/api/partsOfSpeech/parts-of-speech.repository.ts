import PartOfSpeech from "./schemas/part-of-speech.schema";

export async function getPartsOfSpeech () {
    var partsOfSpeech = await PartOfSpeech.find({});
    return partsOfSpeech;
}