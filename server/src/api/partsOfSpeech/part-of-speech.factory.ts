import PartofSpeech from "./interfaces/part-of-speech.interface";

export default function partOfSpeechFactory ({
    _doc: {
        _id,
        name
    }
}: any): PartofSpeech {
    return {
        _id,
        name
    };
}