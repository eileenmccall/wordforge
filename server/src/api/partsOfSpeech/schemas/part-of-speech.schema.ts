import mongoose, { Schema } from "mongoose"
import PartOfSpeechDocument from "../interfaces/part-of-speech-document.interface";

var PartOfSpeechSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        lowercase: true
    }
});

var PartOfSpeech = mongoose.model<PartOfSpeechDocument>("PartOfSpeech", PartOfSpeechSchema, "PartsOfSpeech");

export default PartOfSpeech;