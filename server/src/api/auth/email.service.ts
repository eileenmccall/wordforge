import AWS from "aws-sdk";

export async function sendEmail () {
    // var credentials = new AWS.SharedIniFileCredentials({ profile: "default" });
    const {
        AWS_ACCESS_KEY_ID,
        AWS_SECRET_ACCESS_KEY,
        AWS_REGION
    } = process.env;

    AWS.config.update({
        accessKeyId: AWS_ACCESS_KEY_ID,
        secretAccessKey: AWS_SECRET_ACCESS_KEY,
        region: AWS_REGION
    });

    var ses = new AWS.SES({ apiVersion: "2010-12-01" });

    var params = {
        Destination: { /* required */
            ToAddresses: [
                "joshuaamccall@gmail.com",
                /* more items */
            ]
        },
        Message: { /* required */
            Body: { /* required */
                Html: {
                    Charset: "UTF-8",
                    Data: "<h1>Hey! here's an email!</h1>"
                },
                Text: {
                    Charset: "UTF-8",
                    Data: "Hey! here's an email!"
                }
            },
            Subject: {
                Charset: "UTF-8",
                Data: "This is a new email"
            }
        },
        Source: "eileen@wordforge.io", /* required */
    };

    try {
        await ses.sendEmail(params).promise();
    } catch (err) {
        console.error(err);
    }
}
