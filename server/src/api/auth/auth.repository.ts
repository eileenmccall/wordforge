import { UserModel } from "./schemas/user.schema";
import { compare } from "bcryptjs";
import { userFactory } from "../users/user.factory";
import { User } from "./interfaces/user.interface";
import { ObjectID } from "mongodb";
import { sendEmail } from "./email.service";

export async function login (email, password): Promise<User> {
    var user = await UserModel.findOne({
        email: email.toLowerCase().trim()
    });

    if (!user) {
        throw new Error("Could not find user with specified email address");
    }

    var match = await compare(password, user.password);

    if (!match) {
        throw new Error("Password incorrect");
    }

    return userFactory(user);
}

export async function setSecretKey (userID: ObjectID, secretKey: string) {
    var user = await UserModel.findById(userID);
    user.secretKey = secretKey;
    await user.save();
    return;
}

export async function logout (userId: ObjectID) {
    var user = await UserModel.findById(userId);

    user.secretKey = null;
    await user.save();
    return;
}

export async function register (email: string) {
    await sendEmail();
}