import { ObjectID } from "mongodb";

export interface User {
    _id: ObjectID;
    firstName: string;
    lastName: string;
    readonly fullName: string;
    email: string;
    pronouns: {
        subject: string;
        object: string;
        possessive: string;
        possessiveAdj: string;
    }
    activeForgeID: ObjectID;
}