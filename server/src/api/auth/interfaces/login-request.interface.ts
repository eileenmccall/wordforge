import { Request } from "express";
import { AuthCredentials } from "./auth-credentials.interface";

export interface LoginRequest extends Request {
    body: AuthCredentials;
}