import { Document } from "mongoose";
import { ObjectID } from "mongodb";

export interface UserDocument extends Document {
    _id: ObjectID;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    activeForgeID: ObjectID;
    pronouns: {
        subject: string;
        object: string;
        possessive: string;
        possessiveAdj: string;
    }
    secretKey: string;
}