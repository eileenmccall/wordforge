import { Request, Response, NextFunction } from "express";
import { UserModel } from "../schemas/user.schema";
import jwt from "jsonwebtoken";

export async function requireAuth ({ signedCookies }: Request, res: Response, next: NextFunction) {
    var { userId, authToken } = signedCookies;

    if (!userId || !authToken) {
        return return401();
    }

    var user = await UserModel.findById(userId);

    if (!user) {
        return res.status(404).json("User not found");
    }

    try {
        jwt.verify(authToken, user.secretKey);
        next();
    } catch (error) {
        return401();
    }

    function return401 () {
        res
            .clearCookie("authToken")
            .clearCookie("userId")
            .status(401)
            .json({
                message: "You are not permitted to perform this action"
            });
    }
}