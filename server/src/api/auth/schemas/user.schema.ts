import { Schema, model } from "mongoose";
import { UserDocument } from "../interfaces/user-document.interface";
import { ObjectID } from "mongodb";

var UserSchema = new Schema({
    firstName: {
        type: String,
        required: true,
        trim: true,
        lowercase: false
    },
    lastName: {
        type: String,
        required: true,
        trim: true,
        lowercase: false
    },
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    pronouns: {
        subject: {
            type: String
        },
        object: {
            type: String
        },
        possessive: {
            type: String
        },
        possessiveAdj: {
            type: String
        }
    },
    secretKey: {
        type: String,
        required: false
    },
    activeForgeID: {
        type: ObjectID,
        required: false
    }
}, {
    collection: "Users"
});

export var UserModel = model<UserDocument>("User", UserSchema, "Users");