import { Router, Response, Request } from "express";
import { LoginRequest } from "./interfaces/login-request.interface";
import * as authRepository from "./auth.repository";
import jwt from "jsonwebtoken";
import secureRandom from "secure-random";
import { requireAuth } from "./middleware/auth.middleware";
import * as usersRepository from "../users/users.repository";
import { RegisterRequest } from "./interfaces/register-request.interface";

export default async function authController (app: Router) {
    var route = Router();

    app.use("/auth", route);

    route.post("/login", async function login (req: LoginRequest, res: Response) {

        var { email, password } = req.body;

        if (!email || !password) {
            return res.status(400).json({
                message: "Invalid username or password"
            });
        }

        if (
            typeof email !== "string" ||
            typeof password != "string"
        ) {
            return res.status(400).json({
                message: "Invalid argument, email and password must be strings"
            });
        }

        try {
            var user = await authRepository.login(email, password);
        } catch ({ message }) {
            return res.status(500).json({
                message
            });
        }

        var secretKey = secureRandom(256, { type: "Buffer" }).toString("base64");
        var authToken = jwt.sign({ _id: user._id }, secretKey, { expiresIn: 60 * 60 * 24 * 7 });

        await authRepository.setSecretKey(user._id, secretKey);

        res.cookie("authToken", authToken, {
            httpOnly: true,
            signed: true,
            secure: process.env.NODE_ENV == "production"
        });

        res.cookie("userId", user._id, {
            httpOnly: true,
            signed: true,
            secure: process.env.NODE_ENV == "production"
        });

        return res.json(user).status(200);
    });

    route.post(
        "/logout", requireAuth, async function logout ({ signedCookies }: Request, res) {
        var { userId } = signedCookies;

        await authRepository.logout(userId);

        res.clearCookie("userId");
        res.clearCookie("authToken");

        res.status(200).json({
            message: "You have successfully logged out"
        });
    });

    route.post("/register", async function register (req: RegisterRequest, res) {
        var { email } = req.body;

        if (!email) {
            return res.status(400).json({
                message: "Email is required"
            });
        }

        await authRepository.register(email);

        return res.status(200).json({
            message: "Sending email"
        });
    });

    route.get(
        "/me",
        requireAuth,
        async function getMe ({ signedCookies }, res) {
            var { userId } = signedCookies;

            var user = await usersRepository.getUserById(userId);

            res.status(200).json(user);
        }
    );
}