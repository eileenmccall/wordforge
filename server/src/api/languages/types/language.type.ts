import { ObjectID } from "mongodb";

export type Language = {
    _id: ObjectID,
    name: string;
    code: string;
}