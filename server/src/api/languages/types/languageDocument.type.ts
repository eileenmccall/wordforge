import { Document } from "mongoose";

export type LanguageDocument = {
    name: string;
    code: string;
} & Document;