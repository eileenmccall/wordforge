import mongoose, { Schema } from "mongoose";
import { LanguageDocument } from "../types/languageDocument.type";

var LanguageSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        lowercase: true
    },
    code: {
        type: String,
        required: true,
        trim: true,
        lowercase: true
    }
});

var Language = mongoose.model<LanguageDocument>("Language", LanguageSchema, "Languages");

export default Language;