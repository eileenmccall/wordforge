import Language from "./schemas/language.schema";


export async function getLanguages () {
    var languages = await Language.find({});
    return languages;
}