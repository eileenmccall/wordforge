import { Router, Request, Response } from "express";
import { getLanguages } from "./languages.repository";

export default async function languagesController (app: Router) {
    var route = Router();

    app.use("/languages", route);

    route.get("", async function getLanguagesHandler (req: Request, res: Response) {
        var languages = await getLanguages();
        return res.json(languages).status(200);
    });

    return null;
}