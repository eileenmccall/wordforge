import mongoose from "mongoose";
mongoose.Promise = global.Promise;

export default async function loadMongo (): Promise<void> {
    mongoose.set("debug", true);

    const url = "mongodb+srv://dbtestuser:test123@wordforgedb-iepqg.mongodb.net/Wordforge";

    try {
        console.log("Attempting to connect to database...");
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            dbName: "Wordforge"
        });
        console.log("Connected to database!");
    } catch (error) {
        console.log(error);
        throw new Error("Could not connect to database.");
    }
}
