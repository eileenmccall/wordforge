import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";

import routes from "../api";

export default function expressLoader (app: express.Application): express.Application {

    // Enable CORS
    app.use(cors({
        allowedHeaders: [
            "Origin",
            "X-Requested-With",
            "Content-Type",
            "Accept",
            "X-Access-Token",
            "Authorization"
        ],
        credentials: true,
        methods: "GET, HEAD, OPTIONS, PUT, PATCH, POST, DELETE",
        origin: "http://localhost:3000",
        preflightContinue: false,
    }));

    app.use(cookieParser("82e4e438a0705fabf61f9854e3b575af"));

    // Health check endpoint
    app.get("/status", function handleStatusRequest (req, res): void {
        res.status(200).end();
    });

    app.head("/status", function headStatusRequest (req, res): void {
        res.status(200).end();
    });

    // Middleware that transforms the raw string of req.body into json
    app.use(bodyParser.json());

    // Mount API routes
    app.use("/api", routes());

      // catch 404 and forward to error handler
    app.use(function handle404 (req, res, next): void {
        const err = new Error("Not Found");
        err["status"] = 404;
        next(err);
    });

    app.use(function handle401 (err, req, res, next): void {
        /**
         * Handle 401 thrown by express-jwt library
         */
        if (err.name === "UnauthorizedError") {
            return res
                .status(err.status)
                .send({ message: err.message })
                .end();
        }

        return next(err);
    });

    app.use(function handleError (err, req, res, next): void {
        res.status(err.status || 500);
        res.json({
            errors: {
                message: err.message,
            },
        });

        return;
    });

    return app;
}