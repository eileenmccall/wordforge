import express from "express";
import { Db } from "mongodb";

import expressLoader from "./express.loader";
import loadMongo from "./mongo.loader";

export default async function applyLoaders (app: express.Application): Promise<{app: express.Application, db: Db}> {
    expressLoader(app);
    console.log("Express loaded!")

    var db;
    try {
        db = await loadMongo();
        console.log("MongoDB loaded!")
    } catch (error) {
        console.log(error.message);
        console.log(JSON.stringify(error))
    }

    return {
        app,
        db
    };
}