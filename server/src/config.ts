// var ip = require('ip');
// console.log(ip.address());

export const PORT: number = normalizePort(process.env.PORT) || 8080;

export const HOST: string = "0.0.0.0";

function normalizePort (val) {
    var normalizedPort = parseInt(val, 10);

    if (Number.isNaN(normalizedPort)) {
        // named pipe
        return val;
    }

    if (normalizedPort >= 0) {
        // port number
        return normalizedPort;
    }

    return false;
}