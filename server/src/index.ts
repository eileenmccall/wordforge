import express from "express";
import loaders from "./loaders";
import { PORT } from "./config";
import http from "http";

async function startServer () {

    // Initialize app
    var app = express();
    console.log("App initialized!");

    // Loaders
    await loaders(app);

    var port = PORT;
    app.set("port", port);

    // Listen
    var server = http.createServer(app);
    server.on("error", onError);
    server.on("listening", onListening);
    server.listen(port);

    function onError (error) {
        if (error.syscall !== "listen") {
            throw error;
        }

        {
            let addr = server.address();
            var bind = (typeof addr === "string") ? ("pipe " + addr) : ("port " + port);
        }

        switch (error.code) {
            case "EACCES":
                console.error(bind + " requires elevated privileges");
                process.exit(1);
                break;
            case "EADDRINUSE":
                console.error(bind + " is already in use");
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    function onListening () {
        {
            let addr = server.address();
            var bind = (typeof addr === "string") ? ("pipe " + addr) : ("port " + port);
        }
        console.log(`Listening on ${bind}`);
    }
}

startServer();
