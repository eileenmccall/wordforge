FROM node:12.10.0

WORKDIR /usr/app

COPY package*.json ./
RUN npm ci -qy

CMD ["nodemon", "src/index.js"]

