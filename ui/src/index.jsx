import React from "react";
import ReactDOM from "react-dom";
import "./sass/index.scss";
import App from "./app/App";
import { Provider } from "react-redux/dist/react-redux";
import { configureStore } from "./app/store";

var store = configureStore();
window.soundManager.setup({ debugMode: false });

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("root")
);
