import { ofType } from "redux-observable";
import { LOGIN_REQUEST, loginSuccess, loginFailure, GET_USER_REQUEST, getUserSuccess, getUserFailure, LOGOUT_REQUEST, logoutSuccess, logoutError, REGISTER_REQUEST, registerSuccess, registerFailure, setUserForge } from "./auth.actions";
import { withLatestFrom, map, switchMap, catchError } from "rxjs/operators";
import { login$, getUser$, logout$, register$ } from "./auth.service";
import { of } from "rxjs";
import { SWITCH_ACTIVE_FORGE_SUCCESS } from "../forges/forges.actions";

function loginEpic$ (action$, state$) {
    return action$.pipe(
        ofType(LOGIN_REQUEST),
        withLatestFrom(state$),
        map(function ([ { payload } ]) {
            return payload;
        }),
        switchMap(login$),
        map(loginSuccess),
        catchError(function dispatchErrorAction (error) {
            console.log(error);
            return of(loginFailure(error));
        })
    );
}

function logoutEpic$ (action$, state$) {
    return action$.pipe(
        ofType(LOGOUT_REQUEST),
        switchMap(logout$),
        map(logoutSuccess),
        catchError(function (error) {
            return of(logoutError(error));
        })
    );
}

function registerEpic$ (action$, state$) {
    return action$.pipe(
        ofType(REGISTER_REQUEST),
        map(function (action) {
            return action.payload;
        }),
        switchMap(register$),
        map(registerSuccess),
        catchError(function (error) {
            return of(registerFailure(error));
        })
    );
}

function getUserEpic$ (action$, state$) {
    return action$.pipe(
        ofType(GET_USER_REQUEST),
        switchMap(getUser$),
        map(getUserSuccess),
        catchError(function (error) {
            return of(getUserFailure(error));
        })
    );
}

function setUserForgeEpic$ (actions$, state$) {
    return actions$.pipe(
        ofType(SWITCH_ACTIVE_FORGE_SUCCESS),
        map((action) => setUserForge(action.payload))
    );
}

export default [
    loginEpic$,
    logoutEpic$,
    getUserEpic$,
    registerEpic$,
    setUserForgeEpic$
];