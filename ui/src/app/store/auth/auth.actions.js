import { createActionCreator } from "../utils/actions";

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

export const REGISTER_REQUEST = "REGISTER_REQUEST";
export const REGISTER_SUCCESS = "REGISTER_REQUEST";
export const REGISTER_FAILURE = "REGISTER_REQUEST";

export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_ERROR = "LOGOUT_ERROR";

export const GET_USER_REQUEST = "GET_USER_REQUEST";
export const GET_USER_SUCCESS = "GET_USER_SUCCESS";
export const GET_USER_FAILURE = "GET_USER_FAILURE";

export const SET_USER_FORGE = "SET_USER_FORGE";

export function loginRequest (credentials) {
    return {
        type: LOGIN_REQUEST,
        payload: credentials
    };
}

export function loginSuccess (user) {
    return {
        type: LOGIN_SUCCESS,
        payload: user
    };
}

export function loginFailure (error) {
    return {
        type: LOGIN_FAILURE,
        payload: error
    };
}

export function logoutRequest () {
    return {
        type: LOGOUT_REQUEST
    };
}

export function logoutSuccess () {
    return {
        type: LOGOUT_SUCCESS
    };
}

export function logoutError () {
    return {
        type: LOGOUT_ERROR
    };
}

export function registerRequest (credentials) {
    return {
        type: REGISTER_REQUEST,
        payload: credentials
    };
}

export function registerSuccess () {
    return {
        type: REGISTER_SUCCESS,
        payload: null
    };
}

export var registerFailure = createActionCreator(REGISTER_FAILURE);

export function getUserRequest () {
    return {
        type: GET_USER_REQUEST
    };
}

export function getUserSuccess (user) {
    return {
        type: GET_USER_SUCCESS,
        payload: user
    };
}

export function getUserFailure (error) {
    return {
        type: GET_USER_FAILURE,
        payload: error
    };
}

export function setUserForge (forgeID) {
    return {
        type: SET_USER_FORGE,
        payload: forgeID
    };
}