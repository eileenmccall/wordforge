import * as loginActions from "./auth.actions";

var initialAuthState = {
    loading: false,
    error: null,

    loadingUser: false,
    userError: null,

    loginError: null,
    loadingLogin: false,

    loadingRegister: false,
    registerError: null,

    authenticated: false,
    user: null
};

export function authReducer (state = initialAuthState, action) {
    switch (action.type) {
        case loginActions.LOGIN_REQUEST:
            return {
                ...state,
                loadingLogin: true,
                loginError: null
            };

        case loginActions.LOGIN_SUCCESS:
            return {
                ...state,
                loadingLogin: false,
                authenticated: true,
                user: action.payload
            };

        case loginActions.LOGIN_FAILURE:
            return {
                ...state,
                loadingLogin: false,
                loginError: action.payload
            };

        case loginActions.LOGOUT_REQUEST:
            return {
                ...state,
                loading: true
            };

        case loginActions.LOGOUT_SUCCESS:
            return {
                ...state,
                loading: false,
                authenticated: false,
                user: null
            };

        case loginActions.LOGOUT_ERROR:
            return {
                ...state,
                loading: false,
                authenticated: false,
                user: null,
                error: action.payload.message
            };

        case loginActions.GET_USER_REQUEST:
            return {
                ...state,
                loadingUser: true
            };

        case loginActions.GET_USER_SUCCESS:
            return {
                ...state,
                loadingUser: false,
                authenticated: true,
                user: action.payload
            };

        case loginActions.GET_USER_FAILURE:
            return {
                ...state,
                loadingUser: false,
                userError: action.payload.message
            };

        case loginActions.REGISTER_REQUEST:
            return {
                ...state,
                loadingRegister: true,
                registerError: null
            };

        case loginActions.REGISTER_SUCCESS:
            return {
                ...state,
                loadingRegister: false
            };

        case loginActions.REGISTER_FAILURE:
            return {
                ...state,
                loadingRegister: false,
                registerError: action.payload.message
            };

        case loginActions.SET_USER_FORGE:
            return {
                ...state,
                user: {
                    ...state.user,
                    activeForgeID: action.payload
                }
            };

        default:
            return state;
    }
}