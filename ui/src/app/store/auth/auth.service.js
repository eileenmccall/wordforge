import { http } from "../../shared";

export function login$ (credentials) {
    return http.post$(
        "http://localhost:8080/api/auth/login",
        credentials
    );
}

export function logout$ () {
    return http.post$(
        "http://localhost:8080/api/auth/logout"
    );
}

export function register$ (email) {
    return http.post$(
        "http://localhost:8080/api/auth/register",
        { email }
    );
}

export function getUser$ () {
    return http.get$(
        "http://localhost:8080/api/auth/me"
    );
}