import { createSelector } from "reselect";

export function getAuthState (state) {
    return state.auth;
}

export var getIsAuthenticated = createSelector(
    [ getAuthState ],
    function ({
        authenticated
    }) {
        return authenticated;
    }
);

export var selectLoadingUser = createSelector(
    [ getAuthState ],
    function ({
        loadingUser
    }) {
        return loadingUser;
    }
);

export var selectLoadingLogin = createSelector(
    [ getAuthState ],
    function ({
        loadingLogin
    }) {
        return loadingLogin;
    }
);

export var selectLoginError = createSelector(
    [ getAuthState ],
    function ({
        loginError
    }) {
        return loginError;
    }
);

export var selectUser = createSelector(
    [ getAuthState ],
    (state) => state.user
);

export var selectUserForgeID = createSelector(
    [ selectUser ],
    (user) => user ? user.activeForgeID : null
);