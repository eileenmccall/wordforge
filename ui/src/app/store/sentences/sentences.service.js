import { of } from "rxjs";
import { timeout } from "rxjs/operators";

export function getSentences$ () {
    return of([
        {
            _id: 1,
            raw: "Tha Calum agus Eilidh snog.",
            translation: "Calum and Eilidh are nice.",
            numberOfExercises: 0,
            numberOfWords: 2,
            reviewPercentage: 0
        }, {
            _id: 2,
            raw: "Tha cat agus cù beag.",
            translation: "A cat and a dog are small.",
            numberOfExercises: 0,
            numberOfWords: 3,
            reviewPercentage: 0
        }, {
            _id: 3,
            raw: "Tha Anna agus Calum beag.",
            translation: "Anna and Calum are small.",
            numberOfExercises: 0,
            numberOfWords: 2,
            reviewPercentage: 0
        }
    ]).pipe(timeout(500));
}