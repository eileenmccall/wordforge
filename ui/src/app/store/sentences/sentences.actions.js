export const LOAD_SENTENCES_REQUEST = "LOAD_SENTENCES_REQUEST";
export const LOAD_SENTENCES_SUCCESS = "LOAD_SENTENCES_SUCCESS";
export const LOAD_SENTENCES_FAILURE = "LOAD_SENTENCES_FAILURE";

export const ADD_SENTENCE_REQUEST = "ADD_SENTENCE_REQUEST";
export const ADD_SENTENCE_SUCCESS = "ADD_SENTENCE_SUCCESS";
export const ADD_SENTENCE_FAILURE= "ADD_SENTENCE_FAILURE";

export const EDIT_SENTENCE_REQUEST = "EDIT_SENTENCE_REQUEST";
export const EDIT_SENTENCE_SUCCESS = "EDIT_SENTENCE_SUCCESS";
export const EDIT_SENTENCE_FAILURE = "EDIT_SENTENCE_FAILURE";

export const DELETE_SENTENCE_REQUEST = "DELETE_SENTENCE_REQUEST";
export const DELETE_SENTENCE_SUCCESS = "DELETE_SENTENCE_SUCCESS";
export const DELETE_SENTENCE_FAILURE = "DELETE_SENTENCE_FAILURE";

export const PAGINATE_SENTENCES = "PAGINATE_SENTENCES";

export function loadSentences () {
    return {
        type: LOAD_SENTENCES_REQUEST
    };
}

export function loadSentencesSuccess (sentences) {
    return {
        type: LOAD_SENTENCES_SUCCESS,
        payload: sentences
    };
}

export function loadSentencesFailure (error) {
    return {
        type: LOAD_SENTENCES_FAILURE,
        payload: error
    };
}

export function addSentenceRequest (sentence) {
    return {
        type: ADD_SENTENCE_REQUEST,
        payload: sentence
    };
}

export function paginateSentences (index) {
    return {
        type: PAGINATE_SENTENCES,
        payload: index
    };
}
