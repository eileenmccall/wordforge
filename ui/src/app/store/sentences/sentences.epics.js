import { LOAD_SENTENCES_REQUEST, loadSentencesSuccess, loadSentencesFailure } from "./sentences.actions";
import { ofType } from "redux-observable";
import { withLatestFrom, filter, switchMap, map, catchError } from "rxjs/operators";
import { getSentences$ } from "./sentences.service";
import { of } from "rxjs";

function getSentencesEpic$ (action$, state$) {
    return action$.pipe(
        ofType(LOAD_SENTENCES_REQUEST),
        withLatestFrom(state$),
        filter(function ([ , state ]) {
            return !state.sentences.loaded;
        }),
        switchMap(function () {
            return getSentences$();
        }),
        map(function (sentences) {
            return loadSentencesSuccess(sentences);
        }),
        catchError(function (error) {
            return of(loadSentencesFailure(error));
        })
    );
}

export default [
    getSentencesEpic$
];
