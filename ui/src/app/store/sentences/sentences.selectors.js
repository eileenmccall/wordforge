import { createSelector } from "reselect";
import { sortObjectArrayAscending, sortObjectArrayDescending, mapDictionaryToArray } from "../utils";

export function getSentencesState (state) {
    return state.sentences;
}

export var getSortedSentences = createSelector(
    [ getSentencesState ],
    function sortSentences ({
        sentences,
        sortDirection,
        sortField,
        pageIndex,
        pageSize
    } = {}) {

        var sentencesArray = mapDictionaryToArray(sentences);

        if (sortDirection == "ASC") {
            sentencesArray = sortObjectArrayAscending(sentencesArray, sortField);
        } else {
            sentencesArray = sortObjectArrayDescending(sentencesArray, sortField);
        }

        var start = pageSize * pageIndex;
        var end = start + pageSize;

        return sentencesArray.slice(start, end);
    }
);