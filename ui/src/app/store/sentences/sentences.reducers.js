import { LOAD_SENTENCES_REQUEST, LOAD_SENTENCES_SUCCESS, LOAD_SENTENCES_FAILURE, ADD_SENTENCE_REQUEST, PAGINATE_SENTENCES } from "./sentences.actions";
import { mapArrayToDictionary } from "../utils";

var initialState = {
    sentences: {},
    loading: false,
    loaded: false,
    error: null,
    sortDirection: "ASC",
    sortField: "raw",
    pageSize: 10,
    pageIndex: 0
};

export function sentencesReducer (state = initialState, action) {
    switch (action.type) {
        case LOAD_SENTENCES_REQUEST:
            return {
                ...state,
                loading: true
            };

        case LOAD_SENTENCES_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                sentences: mapArrayToDictionary(action.payload)
            };

        case LOAD_SENTENCES_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };

        case ADD_SENTENCE_REQUEST:
            return {
                ...state,
                sentences: {
                    ...state.sentences,
                    [action.payload._id]: action.payload
                }
            };

        case PAGINATE_SENTENCES:
            return {
                ...state,
                pageIndex: action.payload
            };

        default:
            return state;
    }
}

