import { ofType } from "redux-observable";
import { of } from "rxjs";
import { switchMap, map, catchError, withLatestFrom } from "rxjs/operators";
import {
    LOAD_PARTS_OF_SPEECH_REQUEST,
    loadPartOfSpeechSuccess,
    loadPartOfSpeechFailure,
    GET_LANGUAGES_REQUEST,
    getLanguagesSuccess,
    getLanguagesFailure
} from "./root.actions";
import { getPartsOfSpeech$, getLanguages$ } from "./root.service";

function getPartsOfSpeechEpic$ (action$, state$) {
    return action$.pipe(
        ofType(LOAD_PARTS_OF_SPEECH_REQUEST),
        withLatestFrom(state$),
        switchMap(getPartsOfSpeech$),
        map(function (partsOfSpeech) {
            return loadPartOfSpeechSuccess(partsOfSpeech);
        }),
        catchError(function (error) {
            return of(loadPartOfSpeechFailure(error));
        })
    );
}

function getLanguagesEpic$ (action$, state$) {
    return action$.pipe(
        ofType(GET_LANGUAGES_REQUEST),
        withLatestFrom(state$),
        switchMap(getLanguages$),
        map(function (languages) {
            return getLanguagesSuccess(languages);
        }),
        catchError(function (error) {
            return of(getLanguagesFailure(error));
        })
    );
}

export default [
    getPartsOfSpeechEpic$,
    getLanguagesEpic$
];
