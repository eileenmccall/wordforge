import { createSelector } from "reselect";
// import {
//     sortObjectArrayAscending,
//     sortObjectArrayDescending,
//     mapDictionaryToArray
// } from "../utils";

export function selectRootState (state) {
    return state.app;
}

export var selectPartsOfSpeech = createSelector(
    [ selectRootState ],
    function sortWords ({
        partsOfSpeech
    } = {}) {
        return partsOfSpeech;
    }
);

export var selectLanguages = createSelector(
    [ selectRootState ],
    function getLanguages ({
        languages
    } = {}) {
        return languages;
    }
);
