import { http } from "../../shared";

const PARTS_OF_SPEECH_EP = "http://localhost:8080/api/partsOfSpeech";
const LANGUAGES_EP = "http://localhost:8080/api/languages";

export function getPartsOfSpeech$ () {
    return http.get$(PARTS_OF_SPEECH_EP);
}

export function getLanguages$ () {
    return http.get$(LANGUAGES_EP);
}
