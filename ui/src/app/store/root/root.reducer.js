import {
    INITIAL_ACTION,
    LOAD_PARTS_OF_SPEECH_REQUEST,
    LOAD_PARTS_OF_SPEECH_SUCCESS,
    LOAD_PARTS_OF_SPEECH_FAILURE,
    GET_LANGUAGES_REQUEST,
    GET_LANGUAGES_FAILURE,
    GET_LANGUAGES_SUCCESS
} from "./root.actions";

const initialState = {
    loading: false,
    loaded: false,
    error: null,

    message: "",
    partsOfSpeech: [],
    languages: []
};

export function appReducer (state = initialState, action) {
    switch (action.type) {
        case LOAD_PARTS_OF_SPEECH_REQUEST:
        case GET_LANGUAGES_REQUEST:
            return {
                ...state,
                loading: true
            };

        case LOAD_PARTS_OF_SPEECH_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                partsOfSpeech: action.payload
            };

        case GET_LANGUAGES_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                languages: action.payload
            };

        case LOAD_PARTS_OF_SPEECH_FAILURE:
        case GET_LANGUAGES_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };

        case INITIAL_ACTION:
            return {
                ...state,
                message: action.payload
            };

        default:
            return state;
    }
}
