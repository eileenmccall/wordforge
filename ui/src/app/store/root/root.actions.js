export const INITIAL_ACTION = "INITIAL_ACTION";

export const LOAD_PARTS_OF_SPEECH_REQUEST = "LOAD_PARTS_OF_SPEECH_REQUEST";
export const LOAD_PARTS_OF_SPEECH_SUCCESS = "LOAD_PARTS_OF_SPEECH_SUCCESS";
export const LOAD_PARTS_OF_SPEECH_FAILURE = "LOAD_PARTS_OF_SPEECH_FAILURE";

export const GET_LANGUAGES_REQUEST = "GET_LANGUAGES_REQUEST";
export const GET_LANGUAGES_SUCCESS = "GET_LANGUAGES_SUCCESS";
export const GET_LANGUAGES_FAILURE = "GET_LANGUAGES_FAILURE";

export function initialAction () {
    return {
        type: INITIAL_ACTION,
        payload: "store initialized!"
    };
}

export function loadPartsOfSpeechRequest () {
    return {
        type: LOAD_PARTS_OF_SPEECH_REQUEST,
        payload: null
    };
}

export function loadPartOfSpeechSuccess (partsOfSpeech) {
    return {
        type: LOAD_PARTS_OF_SPEECH_SUCCESS,
        payload: partsOfSpeech
    };
}

export function loadPartOfSpeechFailure (error) {
    return {
        type: LOAD_PARTS_OF_SPEECH_FAILURE,
        payload: error
    };
}

export function getLanguagesRequest () {
    return {
        type: GET_LANGUAGES_REQUEST,
        payload: null
    };
}

export function getLanguagesSuccess (partsOfSpeech) {
    return {
        type: GET_LANGUAGES_SUCCESS,
        payload: partsOfSpeech
    };
}

export function getLanguagesFailure (error) {
    return {
        type: GET_LANGUAGES_FAILURE,
        payload: error
    };
}