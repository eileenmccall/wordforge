import { ofType } from "redux-observable";
import {
    GET_WORDS_REQUEST,
    getWordsSuccess,
    getWordsFailure,
    ADD_WORD_REQUEST,
    addWordFailure
} from "./words.actions";
import { of } from "rxjs";
import { switchMap, map, catchError, withLatestFrom } from "rxjs/operators";
import { getWords$, addWord$ } from "./words.service";
import { dispatchErrorAction } from "../utils/actions";

function getWordsEpic$ (action$, state$) {
    return action$.pipe(
        ofType(GET_WORDS_REQUEST),
        withLatestFrom(state$),
        switchMap(function ([ , state ]) {
            return getWords$(state.forges.activeForge._id);
        }),
        map(function (words) {
            return getWordsSuccess(words);
        }),
        catchError(function (error) {
            return of(getWordsFailure(error));
        })
    );
}

function addWordEpic$ (action$, state$) {
    return action$.pipe(
        ofType(ADD_WORD_REQUEST),
        withLatestFrom(state$),
        map(function mapForgeID ([ action, state ]) {
            return {
                ...action.payload,
                forgeID: state.forges.activeForge._id
            };
        }),
        switchMap(addWord$),
        catchError(dispatchErrorAction(addWordFailure))
    );
}

export default [
    getWordsEpic$,
    addWordEpic$
];
