import {
    GET_WORDS_REQUEST,
    GET_WORDS_SUCCESS,
    GET_WORDS_FAILURE,
    ADD_WORD_REQUEST,
    PAGINATE
} from "./words.actions";

var initialState = {
    words: [],
    loading: false,
    loaded: false,
    error: null,
    sortField: "root",
    sortDirection: "ASC",
    pageSize: 10,
    pageIndex: 0
};

export function wordsReducer (state = initialState, action) {
    switch (action.type) {
        case GET_WORDS_REQUEST:
            return {
                ...state,
                loading: true
            };

        case GET_WORDS_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                words: action.payload
            };

        case GET_WORDS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };

        case ADD_WORD_REQUEST:
            return {
                ...state,
                words: {
                    ...state.words,
                    [action.payload._id]: action.payload
                }
            };

        case PAGINATE:
            return {
                ...state,
                pageIndex: action.payload
            };

        default:
            return state;
    }
}