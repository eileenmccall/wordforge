import { http } from "../../shared";

const WORDS_EP = "http://localhost:8080/api/words";
const PARTS_OF_SPEECH_EP = "http://localhost:8080/api/partsOfSpeech";

export function getWords$ (forgeID) {
    return http.get$(`${WORDS_EP}/${forgeID}`);
}

export function addWord$ (word){
    return http.post$(WORDS_EP, word);
}

export function getPartsOfSpeech$ () {
    return http.get$(PARTS_OF_SPEECH_EP);
}