export const GET_WORDS_REQUEST = "GET_WORDS_REQUEST";
export const GET_WORDS_SUCCESS = "GET_WORDS_SUCCESS";
export const GET_WORDS_FAILURE = "GET_WORDS_FAILURE";

export const ADD_WORD_REQUEST = "ADD_WORD_REQUEST";
export const ADD_WORD_SUCCESS = "ADD_WORD_SUCCESS";
export const ADD_WORD_FAILURE= "ADD_WORD_FAILURE";

export const EDIT_WORD_REQUEST = "EDIT_WORD_REQUEST";
export const EDIT_WORD_SUCCESS = "EDIT_WORD_SUCCESS";
export const EDIT_WORD_FAILURE = "EDIT_WORD_FAILURE";

export const DELETE_WORD_REQUEST = "DELETE_WORD_REQUEST";
export const DELETE_WORD_SUCCESS = "DELETE_WORD_SUCCESS";
export const DELETE_WORD_FAILURE = "DELETE_WORD_FAILURE";

export const GENERATE_WORD_EXERCISES = "GENERATE_WORD_EXERCISES";

export const PAGINATE = "PAGINATE";

export function getWords () {
    return {
        type: GET_WORDS_REQUEST
    };
}

export function getWordsSuccess (words) {
    return {
        type: GET_WORDS_SUCCESS,
        payload: words
    };
}

export function getWordsFailure (error) {
    return {
        type: GET_WORDS_FAILURE,
        payload: error
    };
}

export function addWordRequest (word) {
    return {
        type: ADD_WORD_REQUEST,
        payload: word
    };
}

export function addWordSuccess (createdWord) {
    return {
        type: ADD_WORD_SUCCESS,
        payload: createdWord
    };
}

export function addWordFailure (error) {
    return {
        type: ADD_WORD_FAILURE,
        payload: error
    };
}

export function paginate (index) {
    return {
        type: PAGINATE,
        payload: index
    };
}

export function generateWordExercises (word) {
    return {
        type: GENERATE_WORD_EXERCISES,
        payload: word
    };
}
