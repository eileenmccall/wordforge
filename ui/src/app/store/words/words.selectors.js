import { createSelector } from "reselect";

export function getWordsState (state) {
    return state.words;
}

export var getSortedWords = createSelector(
    [ getWordsState ],
    function sortWords ({
        words,
        // sortDirection,
        // sortField,
        // pageIndex,
        // pageSize
    } = {}) {

        // if (sortDirection == "ASC") {
        //     words = sortObjectArrayAscending(words, sortField);
        // } else {
        //     words = sortObjectArrayDescending(words, sortField);
        // }

        // var start = pageSize * pageIndex;
        // var end = start + pageSize;

        // return words.slice(start, end);

        return words;
    }
);
