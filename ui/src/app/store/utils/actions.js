import { of } from "rxjs";

export var noOpAction = createActionCreator("NO_OP");

export function createActionCreator (type) {
    return function actionCreator (payload) {
        return {
            type,
            payload
        };
    };
}

export function dispatchErrorAction (actionCreator) {
    return function actionDispatcher$ (error) {
        return of(actionCreator(error));
    };
}
