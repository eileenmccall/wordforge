export function getReduce (reduceable) {
    return function reduce (mutator) {
        return mutator(reduceable).state;
    };
}

export function httpRequestMutation ({ state, action }) {
    return {
        state: {
            ...state,
            loading: true,
            loaded: false
        },
        action
    };
}

export function httpSuccessMutation ({ state, action }) {
    return {
        state: {
            ...state,
            loading: false,
            loaded: true
        },
        action
    };
}

export function httpFailureMutation ({ state, action }) {
    return {
        state: {
            ...state,
            loading: false,
            loaded: false,
            error: action.payload
        },
        action
    };
}