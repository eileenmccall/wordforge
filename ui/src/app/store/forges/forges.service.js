import { http } from "../../shared";

const forgesEndpoint = "http://localhost:8080/api/forges";
const usersEndpoint = "http://localhost:8080/api/users";

export function getForges$ () {
    return http.get$(`${forgesEndpoint}`);
}

export function addForge$ (forge) {
    return http.post$(`${forgesEndpoint}`, forge);
}

export function deleteForge$ (forgeID) {
    return http.delete$(`${forgesEndpoint}/${forgeID}`);
}

export function switchActiveForge$ (forgeID) {
    return http.post$(`${usersEndpoint}/activeForge/${forgeID}`);
}