import {
    GET_FORGES_REQUEST,
    GET_FORGES_FAILURE,
    GET_FORGES_SUCCESS,
    UPDATE_FORGE_FAILURE,
    DELETE_FORGE_FAILURE,
    ADD_FORGE_FAILURE,
    ADD_FORGE_REQUEST,
    ADD_FORGE_SUCCESS,
    DELETE_FORGE_REQUEST,
    DELETE_FORGE_SUCCESS,
    SWITCH_ACTIVE_FORGE_REQUEST,
    SWITCH_ACTIVE_FORGE_FAILURE,
    SWITCH_ACTIVE_FORGE_SUCCESS
} from "./forges.actions";

export var initialForgesState = {
    loading: false,
    forgesLoaded: false,

    error: null,
    forgeCreated: false,

    activeForge: null,
    forges: []
};

export function forgesReducer (state = initialForgesState, action) {
    switch (action.type) {
        case GET_FORGES_REQUEST:
        case ADD_FORGE_REQUEST:
            return {
                ...state,
                forgesLoaded: false,
                loading: true
            };

        case GET_FORGES_SUCCESS:
            return {
                ...state,
                loading: false,
                forgesLoaded: true,
                forges: action.payload
            };

        case ADD_FORGE_SUCCESS:
            return {
                ...state,
                loading: false,
                forgeCreated: true,
                forges: [
                    ...state.forges,
                    action.payload,
                ]
            };

        case DELETE_FORGE_REQUEST:
            return {
                ...state,
                loading: true
            };

        case DELETE_FORGE_SUCCESS:
            return {
                ...state,
                loading: false
            };

        case GET_FORGES_FAILURE:
        case ADD_FORGE_FAILURE:
        case UPDATE_FORGE_FAILURE:
        case DELETE_FORGE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };

        case SWITCH_ACTIVE_FORGE_REQUEST:
            return {
                ...state,
                loading: true,
                error: null
            };

        case SWITCH_ACTIVE_FORGE_SUCCESS:
            return {
                ...state,
                loading: false
            };

        case SWITCH_ACTIVE_FORGE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };

        default:
            return state;
    }
}