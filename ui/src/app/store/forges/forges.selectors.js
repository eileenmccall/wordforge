import { createSelector } from "reselect";
import { mapDictionaryToArray } from "../utils";

export function getForgesState (state) {
    return state.forges;
}

export var getSortedForges = createSelector(
    [ getForgesState ],
    function sortForges ({
        forges
    }) {
        return mapDictionaryToArray(forges);
    }
);

export var selectActiveForge = createSelector(
    [ getForgesState ],
    function getActiveForge ({
        activeForge
    }) {
        return activeForge;
    }
);

export function getSelectForgeByIDSelector (forgeID) {
    return createSelector(
        [ getForgesState ],
        function getForgeByID ({ forges = [] }) {
            return forges.find((forge) => forge._id == forgeID);
        }
    );
}

export var selectForgesError = createSelector(
    [ getForgesState ],
    function getForgesError ({
        error
    }) {
        return error;
    }
);

export var selectForgeCreated = createSelector(
    [ getForgesState ],
    function getForgeCreated ({
        forgeCreated
    }) {
        return forgeCreated;
    }
);