import React from "react";

import { ofType } from "redux-observable";
import { switchMap, map, catchError, withLatestFrom } from "rxjs/operators";
import {
    getForgesSuccess,
    getForgesFailure,
    GET_FORGES_REQUEST,
    ADD_FORGE_REQUEST,
    addForgeFailure,
    addForgeSuccess,
    DELETE_FORGE_REQUEST,
    deleteForgeSuccess,
    deleteForgeFailure,
    DELETE_FORGE_SUCCESS,
    ADD_FORGE_SUCCESS,
    ADD_FORGE_FAILURE,
    LOAD_FORGES,
    getForgesRequest,
    DELETE_FORGE_FAILURE,
    SWITCH_ACTIVE_FORGE_REQUEST,
    switchActiveForgeSuccess,
    switchActiveForgeFailure
} from "./forges.actions";
import { getForges$, addForge$, deleteForge$, switchActiveForge$ } from "./forges.service";
import { of } from "rxjs";
import {
    addToast
} from "../toasts/toasts.actions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-regular-svg-icons";
import { noOpAction } from "../utils/actions";
import { faExclamationCircle } from "@fortawesome/free-solid-svg-icons";

function loadForgesEpic$ (actions$, state$) {
    return actions$.pipe(
        ofType(LOAD_FORGES),
        withLatestFrom(state$),
        map(function ([ , state ]) {
            if (!state.forges.forgesLoaded) {
                return getForgesRequest();
            }

            return noOpAction();
        })
    );
}

function getForgesEpic$ (actions$, state$) {
    return actions$.pipe(
        ofType(GET_FORGES_REQUEST, DELETE_FORGE_SUCCESS),
        switchMap(getForges$),
        map(getForgesSuccess),
        catchError(function dispatchErrorAction (error) {
            return of(getForgesFailure(error));
        })
    );
}

function addForgeEpic$ (actions$, state$) {
    return actions$.pipe(
        ofType(ADD_FORGE_REQUEST),
        switchMap(function (action) {
            return addForge$(action.payload);
        }),
        map(addForgeSuccess),
        catchError(function dispatchErrorAction (error) {
            return of(addForgeFailure(error));
        })
    );
}

function addForgeSuccessMessageEpic$ (actions$) {
    return actions$.pipe(
        ofType(ADD_FORGE_SUCCESS),
        map(function () {
            return addToast({
                body: (
                    <>
                        <FontAwesomeIcon style={{ color: "green" }} className="mr-1" icon={faCheckCircle} />
                        Forge successfully created!
                    </>
                )
            });
        })
    );
}

function addForgeFailureMessageEpic$ (actions$) {
    return actions$.pipe(
        ofType(ADD_FORGE_FAILURE),
        map(function () {
            return addToast({
                body: (
                    <>
                        <FontAwesomeIcon style={{ color: "red" }} className="mr-1" icon={faExclamationCircle} />
                        Something went wrong, forge not created!
                    </>
                )
            });
        })
    );
}

function deleteForgeEpic$ (actions$) {
    return actions$.pipe(
        ofType(DELETE_FORGE_REQUEST),
        switchMap(function (action) {
            return deleteForge$(action.payload);
        }),
        map(deleteForgeSuccess),
        catchError(function (error) {
            return of(deleteForgeFailure(error));
        })
    );
}

function deleteForgeSuccessMessageEpic$ (actions$) {
    return actions$.pipe(
        ofType(DELETE_FORGE_SUCCESS),
        map(function () {
            return addToast({
                body: (
                    <>
                        <FontAwesomeIcon style={{ color: "green" }} className="mr-1" icon={faCheckCircle} />
                        Forge successfully deleted!
                    </>
                )
            });
        })
    );
}

function deleteForgeFailureMessageEpic$ (actions$) {
    return actions$.pipe(
        ofType(DELETE_FORGE_FAILURE),
        map(function () {
            return addToast({
                body: (
                    <>
                        <FontAwesomeIcon style={{ color: "red" }} className="mr-1" icon={faExclamationCircle} />
                        Something went wrong, forge not deleted!
                    </>
                )
            });
        })
    );
}

function switchActiveForgeEpic$ (actions$) {
    return actions$.pipe(
        ofType(SWITCH_ACTIVE_FORGE_REQUEST),
        map((action) => action.payload),
        switchMap(switchActiveForge$),
        map((user) => switchActiveForgeSuccess(user.activeForgeID)),
        catchError((error) => of(switchActiveForgeFailure(error)))
    );
}

export default [
    loadForgesEpic$,
    getForgesEpic$,
    addForgeEpic$,
    deleteForgeEpic$,
    addForgeSuccessMessageEpic$,
    addForgeFailureMessageEpic$,
    deleteForgeSuccessMessageEpic$,
    deleteForgeFailureMessageEpic$,
    switchActiveForgeEpic$
];
