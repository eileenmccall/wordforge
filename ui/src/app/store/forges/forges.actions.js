export const LOAD_FORGES = "LOAD_FORGES";

export const GET_FORGES_REQUEST = "GET_FORGES_REQUEST";
export const GET_FORGES_SUCCESS = "GET_FORGES_SUCCESS";
export const GET_FORGES_FAILURE = "GET_FORGES_FAILURE";

export const ADD_FORGE_REQUEST = "ADD_FORGE_REQUEST";
export const ADD_FORGE_SUCCESS = "ADD_FORGE_SUCCESS";
export const ADD_FORGE_FAILURE = "ADD_FORGE_FAILURE";

export const UPDATE_FORGE_REQUEST = "UPDATE_FORGE_REQUEST";
export const UPDATE_FORGE_SUCCESS = "UPDATE_FORGE_SUCCESS";
export const UPDATE_FORGE_FAILURE = "UPDATE_FORGE_FAILURE";

export const DELETE_FORGE_REQUEST = "DELETE_FORGE_REQUEST";
export const DELETE_FORGE_SUCCESS = "DELETE_FORGE_SUCCESS";
export const DELETE_FORGE_FAILURE = "DELETE_FORGE_FAILURE";

export const SWITCH_ACTIVE_FORGE_REQUEST = "SWITCH_ACTIVE_FORGE_REQUEST";
export const SWITCH_ACTIVE_FORGE_SUCCESS = "SWITCH_ACTIVE_FORGE_SUCCESS";
export const SWITCH_ACTIVE_FORGE_FAILURE = "SWITCH_ACTIVE_FORGE_FAILURE";

export function loadForges (source) {
    return {
        type: LOAD_FORGES,
        source
    };
}

export function getForgesRequest () {
    return {
        type: GET_FORGES_REQUEST
    };
}

export function getForgesSuccess (forges) {
    return {
        type: GET_FORGES_SUCCESS,
        payload: forges
    };
}

export function getForgesFailure (error) {
    return {
        type: GET_FORGES_FAILURE,
        payload: error
    };
}

export function switchActiveForgeRequest (forgeID) {
    return {
        type: SWITCH_ACTIVE_FORGE_REQUEST,
        payload: forgeID
    };
}

export function switchActiveForgeSuccess (updatedUser) {
    return {
        type: SWITCH_ACTIVE_FORGE_SUCCESS,
        payload: updatedUser
    };
}

export function switchActiveForgeFailure (error) {
    return {
        type: SWITCH_ACTIVE_FORGE_FAILURE,
        payload: error
    };
}

export function addForgeRequest (forge) {
    return {
        type: ADD_FORGE_REQUEST,
        payload: forge
    };
}

export function addForgeSuccess (createdForge) {
    return {
        type: ADD_FORGE_SUCCESS,
        payload: createdForge
    };
}

export function addForgeFailure (error) {
    return {
        type: ADD_FORGE_FAILURE,
        payload: error
    };
}

export function deleteForgeRequest (forgeID) {
    return {
        type: DELETE_FORGE_REQUEST,
        payload: forgeID
    };
}

export function deleteForgeSuccess (forgeID) {
    return {
        type: DELETE_FORGE_SUCCESS,
        payload: forgeID
    };
}

export function deleteForgeFailure (error) {
    return {
        type: DELETE_FORGE_FAILURE,
        payload: error
    };
}