import * as appActions from "./root/root.actions";
import * as appSelectors from "./root/root.selectors";

import * as authActions from "./auth/auth.actions";
import * as authSelectors from "./auth/auth.selectors";

import * as wordsActions from "./words/words.actions";
import * as wordsSelectors from "./words/words.selectors";

import * as sentencesActions from "./sentences/sentences.actions";
import * as sentencesSelectors from "./sentences/sentences.selectors";

import * as forgesActions from "./forges/forges.actions";
import * as forgesSelectors from "./forges/forges.selectors";

import * as sessionActions from "./session/session.actions";
import * as sessionSelectors from "./session/session.selectors";

export { rootReducer, rootEpic, configureStore } from "./config";
export {
    appActions,
    authActions,
    wordsActions,
    sentencesActions,
    forgesActions,
    sessionActions
};
export {
    appSelectors,
    authSelectors,
    wordsSelectors,
    sentencesSelectors,
    forgesSelectors,
    sessionSelectors
};
