import { combineEpics, createEpicMiddleware } from "redux-observable";
import { combineReducers, createStore, applyMiddleware, compose } from "redux";

import { appReducer } from "./root/root.reducer";
import { authReducer } from "./auth/auth.reducers";
import { wordsReducer } from "./words/words.reducers";
import { sentencesReducer } from "./sentences/sentences.reducers";
import { forgesReducer } from "./forges/forges.reducers";

import appEpics from "./root/root.epics";
import authEpics from "./auth/auth.epics";
import wordsEpics from "./words/words.epics";
import sentencesEpics from "./sentences/sentences.epics";
import forgeEpics from "./forges/forges.epics";
import { sessionReducer } from "./session/session.reducers";
import sessionEpics from "./session/session.epics";
import { toastsReducer } from "./toasts/toasts.reducer";

export var rootEpic = combineEpics(
    ...appEpics,
    ...authEpics,
    ...wordsEpics,
    ...sentencesEpics,
    ...forgeEpics,
    ...sessionEpics
);

export var rootReducer = combineReducers({
    app: appReducer,
    auth: authReducer,
    words: wordsReducer,
    sentences: sentencesReducer,
    forges: forgesReducer,
    session: sessionReducer,
    toasts: toastsReducer
});

export function configureStore () {
    var composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    var epicMiddleware = createEpicMiddleware();

    var store = createStore(
        rootReducer,
        composeEnhancers(
            applyMiddleware(epicMiddleware))
        );

    epicMiddleware.run(rootEpic);

    return store;
}