export function mapArrayToDictionary (array) {
    var dictionary = {};

    array.forEach(function mapById (item) {
        dictionary[item._id] = item;
    });

    return dictionary;
}

export function mapDictionaryToArray (dictionary) {
    var array = Object.keys(dictionary).map(
        function mapWords (_id) {
            return dictionary[_id];
        }
    );

    return array;
}

export function updateOne (array = [], partial = {}, key = "_id") {
    return array.map(function updateEntry (entry) {
        return (entry[key] == partial[key])
            ? ({
                ...entry,
                ...partial
            })
            : (entry);
    });
}

export function sortObjectArrayAscending (array, sortField) {
    return array.sort(function sortAscending (a, b) {
        if (a[sortField] > b[sortField]) {
            return 1;
        } else if (a[sortField] < b[sortField]) {
            return -1;
        } else {
            return 0;
        }
    });
}

export function sortObjectArrayDescending (array, sortField) {
    return array.sort(function sortDescending (a, b) {
        if (a[sortField] > b[sortField]) {
            return 1;
        } else if (a[sortField] < b[sortField]) {
            return -1;
        } else {
            return 0;
        }
    });
}

export function pipe (fns = [], initialState = null) {
    return fns.reduce(function callFn (acc, cur) {
        return cur(acc);
    }, initialState);
}

