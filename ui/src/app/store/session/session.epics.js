import { ofType } from "redux-observable";
import { switchMap, map, catchError, withLatestFrom } from "rxjs/operators";
import { getSession$, answerExercise$, getSessionResults$, markExerciseDifficulty$ } from "./session.service";
import {
    START_SESSION_REQUEST,
    startSessionSuccess,
    startSessionFailure,
    answerExerciseFailure,
    FINISH_SESSION_REQUEST,
    finishSessionSuccess,
    finishSessionFailure,
    ANSWER_EXERCISE_REQUEST,
    MARK_EXERCISE_REQUEST,
    finishSessionRequest,
    getNextExerciseRequest,
    markExerciseFailure,
    answerExerciseSuccess,
    markExerciseSuccess,
    MARK_EXERCISE_SUCCESS
} from "./session.actions";
import { dispatchErrorAction } from "../utils/actions";

function getSessionEpic$ (action$, state$) {
    return action$.pipe(
        ofType(START_SESSION_REQUEST),
        withLatestFrom(state$),
        switchMap(function ([ action, state ]) {
            return getSession$(state.auth.user._id, action.payload);
        }),
        map(startSessionSuccess),
        catchError(dispatchErrorAction(startSessionFailure))
    );
}

function answerExerciseEpic$ (action$, state$) {
    return action$.pipe(
        ofType(ANSWER_EXERCISE_REQUEST),
        withLatestFrom(state$),
        switchMap(function ([ action, state ]) {
            return answerExercise$(state.sessionId, state.exercise._id, action.payload);
        }),
        map(answerExerciseSuccess),
        catchError(dispatchErrorAction(answerExerciseFailure))
    );
}

function markExerciseEpic$ (action$, state$) {
    return action$.pipe(
        ofType(MARK_EXERCISE_REQUEST),
        withLatestFrom(state$),
        switchMap(function ([ action, state ]) {
            return markExerciseDifficulty$(state.sessionId, action.payload);
        }),
        map(markExerciseSuccess),
        catchError(dispatchErrorAction(markExerciseFailure))
    );
}

function getNextExerciseEpic$ (action$, state$) {
    return action$.pipe(
        ofType([ MARK_EXERCISE_SUCCESS ]),
        withLatestFrom(state$),
        map(function ([ , state ]) {
            if (state.completed) {
                return finishSessionRequest();
            }
            return getNextExerciseRequest();
        })
    );
}

function finishSessionEpic$ (action$, state$) {
    return action$.pipe(
        ofType(FINISH_SESSION_REQUEST),
        withLatestFrom(state$),
        switchMap(function ([ , state ]) {
            return getSessionResults$(state.sessionId);
        }),
        map(finishSessionSuccess),
        catchError(dispatchErrorAction(finishSessionFailure))
    );
}

export default [
    getSessionEpic$,
    answerExerciseEpic$,
    markExerciseEpic$,
    getNextExerciseEpic$,
    finishSessionEpic$
];
