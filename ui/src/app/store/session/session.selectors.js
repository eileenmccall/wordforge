import { createSelector } from "reselect";
// import {
//     sortObjectArrayAscending,
//     sortObjectArrayDescending,
//     mapDictionaryToArray
// } from "../utils";

export function selectSessionState (state) {
    return state.session;
}

export var selectSession = createSelector(
    [ selectSessionState ],
    function getSession ({
        session
    } = {}) {
        return session;
    }
);

export var selectSessionExercises = createSelector(
    [ selectSessionState ],
    function getExercises ({
        exercises
    } = {}) {
        return exercises;
    }
);

export var selectExercisesLength = createSelector(
    [ selectSessionExercises ],
    function getLength (exercises = []) {
        return exercises.length;
    }
);

export var selectCurrentAnswer = createSelector(
    [ selectSessionState ],
    function getAnswer ({
        currentExerciseIndex,
        answers = []
    }) {
        return answers[currentExerciseIndex];
    }
);

export var selectSessionResults = createSelector(
    [ selectSessionState ],
    function getResults ({
        results = {}
    }) {
        return results;
    }
);
