import { http } from "../../shared";
import { apiURL } from "../../../environment";
import { of } from "rxjs";

export function getSession$ (userId, sessionConfig) {
    console.log(sessionConfig);
    // return http.post$(
    //     `${apiURL}/sessions`,
    //     sessionConfig,
    //     {
    //         params: {
    //             userId: 1
    //         }
    //     }
    // ).pipe(
    //     map(function mapResponse ({
    //         _id: sessionId,
    //         ...response
    //     }) {
    //         return {
    //             sessionId,
    //             ...response
    //         };
    //     })
    // );

    return of({
        sessionId: "1234",
        complete: false,
        exercise: exercises[3],
        totalExercises: 10
    });
}

export function answerExercise$ (sessionId, exerciseId, answer) {
    return http.post$(
        `${apiURL}/sessions/${sessionId}/exercises/${exerciseId}/answer`,
        answer
    );
}

export function addAcceptedAnswer$ (exerciseId, answer) {
    return http.post$(
        `${apiURL}/exercises/${exerciseId}/answers`,
        answer
    );
}

export function markExerciseDifficulty$ (sessionId, exerciseId, difficultyRating) {
    return http.put$(
        `${apiURL}/sessions/${sessionId}/exercises/${exerciseId}/difficulty`,
        difficultyRating
    );
}

export function getSessionResults$ (sessionId) {
    return http.get$(
        `${apiURL}/session/${sessionId}/results`
    );
}

var exercises = [
    {
        _id: 1,
        isNew: true,
        isFlagged: false,
        isLeech: false,

        type: "translateToTarget",
        sourceLanguage: "English",
        targetLanguage: "French",
        source: "I am eating.",
        acceptableAnswers: [ "Je mange.", "Je mange" ],
        imageURLs: [
            "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F35%2F2019%2F05%2F21181957%2Fwomen-eating-food-hashtag.jpg&q=85",
            "https://www.washingtonpost.com/wp-apps/imrs.php?src=https://arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/P7W7TZS72UI6TPZE3NFZ7NRKUI.jpg&w=767",
            "https://www.health.harvard.edu/media/content/images/woman-late-night-snacking-refridgerator-eating-food-iStock_000069204943_Medium.jpg",
            "https://secure.i.telegraph.co.uk/multimedia/archive/01475/fatKid_1475843c.jpg"
        ],
        audioURLs: [ "https://faclair.com/Listen/uisge-beatha.mp3" ],
        notes: []
    }, {
        _id: 2,
        isNew: false,
        isFlagged: false,
        isLeech: false,

        type: "translateToSource",
        sourceLanguage: "English",
        targetLanguage: "French",
        target: "Je mange.",
        acceptableAnswers: [ "I eat", "I am eating" ],
        imageURLs: [
            "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F35%2F2019%2F05%2F21181957%2Fwomen-eating-food-hashtag.jpg&q=85",
            "https://www.washingtonpost.com/wp-apps/imrs.php?src=https://arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/P7W7TZS72UI6TPZE3NFZ7NRKUI.jpg&w=767",
            "https://www.health.harvard.edu/media/content/images/woman-late-night-snacking-refridgerator-eating-food-iStock_000069204943_Medium.jpg",
            "https://secure.i.telegraph.co.uk/multimedia/archive/01475/fatKid_1475843c.jpg"
        ],
        audioURLs: [],
        notes: []
    },
    {
        _id: 3,
        isNew: false,
        isFlagged: true,
        isLeech: false,

        type: "pictureToWord",
        imageURLs: [
            {
                _id: 1,
                url: "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F35%2F2019%2F05%2F21181957%2Fwomen-eating-food-hashtag.jpg&q=85"
            }, {
                _id: 2,
                url: "https://www.washingtonpost.com/wp-apps/imrs.php?src=https://arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/P7W7TZS72UI6TPZE3NFZ7NRKUI.jpg&w=767"
            }, {
                _id: 3,
                url: "https://www.health.harvard.edu/media/content/images/woman-late-night-snacking-refridgerator-eating-food-iStock_000069204943_Medium.jpg"
            }, {
                _id: 4,
                url: "https://secure.i.telegraph.co.uk/multimedia/archive/01475/fatKid_1475843c.jpg"
            }
        ],
        acceptableAnswers: [ "manger" ],
        audioURLs: [],
        notes: []
    },
    {
        _id: 4,
        isNew: false,
        isFlagged: false,
        isLeech: true,

        type: "wordToPicture",
        target: "manger",
        answerId: 4,
        imageURLs: [
            {
                _id: 1,
                url: "https://queerty-prodweb.s3.amazonaws.com/2019/08/men-who-are-attracted-to-trans-women.jpg"
            }, {
                _id: 2,
                url: "https://media.mnn.com/assets/images/2016/03/kids-laughing.jpg.838x0_q80.jpg"
            }, {
                _id: 3,
                url: "https://www.runtastic.com/blog/wp-content/uploads/2018/04/thumbnail_8-tips-beginner_1200x800-1024x683.jpg"
            }, {
                _id: 4,
                url: "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F35%2F2019%2F05%2F21181957%2Fwomen-eating-food-hashtag.jpg&q=85"
            }
        ],
        audioURLs: [],
        notes: []
    },
    {
        _id: 5,
        isNew: false,
        isFlagged: false,
        isLeech: false,

        type: "transcribeAudio",
        audioURL: "https://faclair.com/Listen/uisge-beatha.mp3",
        acceptableAnswers: [ "uisge-beatha" ]
    },

    {
        _id: 6,
        isNew: false,
        isFlagged: false,
        isLeech: false,

        type: "fillInTheBlank"
    }, {
        _id: 7,
        isNew: false,
        isFlagged: false,
        isLeech: false,

        type: "repeatTheAudio"
    }, {
        _id: 8,
        isNew: false,
        isFlagged: false,
        isLeech: false,

        type: "wordsToSentence"
    }, {
        _id: 9,
        isNew: false,
        isFlagged: false,
        isLeech: false,

        type: "pickWordGender"
    }, {
        _id: 10,
        isNew: false,
        isFlagged: false,
        isLeech: false,

        type: "conjugateWord"
    }
];