export const START_SESSION_REQUEST = "START_SESSION_REQUEST";
export const START_SESSION_SUCCESS = "START_SESSION_SUCCESS";
export const START_SESSION_FAILURE = "START_SESSION_FAILURE";

export const ANSWER_EXERCISE_REQUEST = "ANSWER_EXERCISE_CORRECTLY";
export const ANSWER_EXERCISE_SUCCESS = "ANSWER_EXERCISE_SUCCESS";
export const ANSWER_EXERCISE_FAILURE = "ANSWER_EXERCISE_FAILURE";

export const MARK_EXERCISE_REQUEST = "MARK_EXERCISE_REQUEST";
export const MARK_EXERCISE_SUCCESS = "MARK_EXERCISE_SUCCESS";
export const MARK_EXERCISE_FAILURE = "MARK_EXERCISE_FAILURE";

export const ANSWER_OVERRIDE = "ANSWER_OVERRIDE";
export const ADD_ACCEPTED_ANSWER = "ADD_ACCEPTED_ANSWER";
export const FLAG_EXERCISE = "FLAG_EXERCISE";

export const GET_NEXT_EXERCISE_REQUEST = "GET_NEXT_EXERCISE_REQUEST";
export const GET_NEXT_EXERCISE_SUCCESS = "GET_NEXT_EXERCISE_SUCCESS";
export const GET_NEXT_EXERCISE_FAILURE = "GET_NEXT_EXERCISE_FAILURE";

export const FINISH_SESSION_REQUEST = "FINISH_SESSION_REQUEST";
export const FINISH_SESSION_SUCCESS = "FINISH_SESSION_SUCCESS";
export const FINISH_SESSION_FAILURE = "FINISH_SESSION_FAILURE";

export function startSessionRequest (sessionConfig) {
    return {
        type: START_SESSION_REQUEST,
        payload: sessionConfig
    };
}

export function startSessionSuccess (session) {
    return {
        type: START_SESSION_SUCCESS,
        payload: session
    };
}

export function startSessionFailure (error) {
    return {
        type: START_SESSION_FAILURE,
        payload: error
    };
}

export function answerExerciseRequest (answer) {
    return {
        type: ANSWER_EXERCISE_REQUEST,
        payload: answer
    };
}

export function answerExerciseSuccess (answer) {
    return {
        type: ANSWER_EXERCISE_SUCCESS,
        payload: answer
    };
}

export function answerExerciseFailure (error) {
    return {
        type: ANSWER_EXERCISE_FAILURE,
        payload: error
    };
}

export function markExerciseRequest (difficultyRating) {
    return {
        type: MARK_EXERCISE_REQUEST,
        payload: {
            difficultyRating
        }
    };
}

export function markExerciseSuccess (response) {
    return {
        type: MARK_EXERCISE_SUCCESS,
        payload: response
    };
}

export function markExerciseFailure (error) {
    return {
        type: MARK_EXERCISE_FAILURE,
        payload: error
    };
}

export function getNextExerciseRequest () {
    return {
        type: GET_NEXT_EXERCISE_REQUEST
    };
}


export function overrideAnswer () {
    return {
        type: ANSWER_OVERRIDE,
        payload: {
            correct: true
        }
    };
}

export function finishSessionRequest () {
    return { type: FINISH_SESSION_REQUEST };
}

export function finishSessionSuccess (results) {
    return {
        type: FINISH_SESSION_SUCCESS,
        payload: results
    };
}

export function finishSessionFailure (error) {
    return {
        type: FINISH_SESSION_FAILURE,
        payload: error
    };
}

