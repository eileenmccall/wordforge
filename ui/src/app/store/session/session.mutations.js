import { httpSuccessMutation, httpFailureMutation } from "../utils/mutations";
import { pipe } from "../utils";

export function startSessionSuccessMutation (reduceable) {
    return pipe([
        httpSuccessMutation,
        initializeSessionMuatation
    ], reduceable);
}

export function initializeSessionMuatation ({
    state,
    action: {
        payload: {
            sessionId,
            exercise,
            totalExercises,
            currentExerciseIndex,
            complete
        } = {}
    } = {},
    action
} = {}) {
    return {
        state: {
            ...state,

            reviewing: true,
            sessionId,
            exercise,
            totalExercises,
            currentExerciseIndex,
            complete
        },
        action
    };
}

export function startSessionFailureMutation (reduceable) {
    var { action } = reduceable;

    return {
        state: {
            ...httpFailureMutation(reduceable).state,
            reviewing: false,
        },
        action
    };
}

export function answerExerciseRequestMutation ({ state, action }) {
    return {
        state: {
            ...state,

        },
        action
    };
}

export function markExerciseRequestMutation ([ state, action ]) {
    return {
        state: {
            ...state,
            loadingExercise: true
        },
        action
    };
}

export function finishSessionSuccessMutation ({ state, action } ) {
    return {
        state: {
            ...state,
            complete: true,
            results: action.payload
        },
        action
    };
}

export function exitSessionMutation ({ state, action }) {
    return {
        state: {
            ...state,
            reviewing: false,
            finished: true
        },
        action
    };
}