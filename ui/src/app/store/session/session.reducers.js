import {
    START_SESSION_REQUEST,
    START_SESSION_SUCCESS,
    START_SESSION_FAILURE,
    FINISH_SESSION_REQUEST,
    FINISH_SESSION_SUCCESS,
    FINISH_SESSION_FAILURE,
    MARK_EXERCISE_REQUEST,
    GET_NEXT_EXERCISE_SUCCESS,
    GET_NEXT_EXERCISE_FAILURE,
    ANSWER_EXERCISE_REQUEST,
    ANSWER_EXERCISE_SUCCESS,
    ANSWER_EXERCISE_FAILURE,
} from "./session.actions";

var initialSessionState = {
    loadingSession: false,
    error: null,

    sessionId: null,
    reviewing: false,
    completed: false,

    loadingExercise: false,
    exercise: null,

    totalExercises: null,
    currentExerciseIndex: null,

    loadingAnswer: false,
    answer: null,

    loadingResults: false,
    results: null
};

export function sessionReducer (state = initialSessionState, action = {}) {

    switch (action.type) {
        case START_SESSION_REQUEST:
            return {
                ...state,
                loading: true
            };

        case START_SESSION_SUCCESS:
            return {
                ...state,

                loadingSession: false,
                reviewing: true,
                sessionId: action.payload.sessionId,
                complete: action.payload.complete,

                exercise: action.payload.exercise,
                currentExerciseIndex: action.payload.currentExerciseIndex,
                totalExercises: action.payload.totalExercises,
            };

        case START_SESSION_FAILURE:
            return {
                loadingSession: false,
                error: action.payload
            };

        case ANSWER_EXERCISE_REQUEST:
            return {
                ...state,
                loadingAnswer: true
            };

        case ANSWER_EXERCISE_SUCCESS:
            return {
                ...state,
                loadingAnswer: false,
                answer: action.payload
            };

        case ANSWER_EXERCISE_FAILURE:
            return {
                ...state,
                loadingAnswer: false,
                error: action.payload
            };

        case MARK_EXERCISE_REQUEST:
            return {
                ...state,
                loadingExercise: true,
                answer: null
            };

        case GET_NEXT_EXERCISE_SUCCESS:
            return {
                ...state,
                loadingExercise: false,
                completed: action.payload.completed,
                exercise: action.payload.exercise,
                currentExerciseIndex: action.payload.currentExerciseIndex
            };

        case GET_NEXT_EXERCISE_FAILURE:
            return {
                ...state,
                loadingExercise: false,
                error: action.payload
            };

        case FINISH_SESSION_REQUEST:
            return {
                ...initialSessionState,
                sessionId: state.sessionId,
                loadingResults: true,
            };

        case FINISH_SESSION_SUCCESS:
            return {
                ...state,
                loadingResults: false,
                results: action.payload
            };

        case FINISH_SESSION_FAILURE:
            return {
                ...state,
                loadingResults: false,
                error: action.payload
            };

        default:
            return state;
    }
}

