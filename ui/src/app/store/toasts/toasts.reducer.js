import { ADD_TOAST, REMOVE_TOASTS } from "./toasts.actions";

var initialState = {
    toasts: [],
    id: 1
};

export function toastsReducer (state = initialState, action) {
    switch (action.type) {
        case ADD_TOAST: {
            return {
                toasts: [
                    ...state.toasts,
                    {
                        id: state.id + 1,
                        options: action.payload
                    }
                ],
                id: state.id + 1
            };
        }

        case REMOVE_TOASTS: {
            return {
                ...state,
                toasts: state.toasts.filter((toast) => toast.id != action.payload),
            };
        }

        default: {
            return state;
        }
    }
}