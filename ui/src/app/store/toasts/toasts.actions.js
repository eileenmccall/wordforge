export const ADD_TOAST = "ADD_TOAST";
export const REMOVE_TOASTS = "REMOVE_TOAST";

export function addToast (options = {}) {
    return {
        type: ADD_TOAST,
        payload: options
    };
}

export function removeToast (id) {
    return {
        type: REMOVE_TOASTS,
        payload: id
    };
}

