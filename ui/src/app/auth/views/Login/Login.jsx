import React from "react";
import { Container, Col, Row, Form, Alert } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux/dist/react-redux";
import { Redirect } from "react-router-dom";

import "./Login.scss";
import { Unauthenticated } from "../../../layout";
import { useInput } from "../../../shared";
import { authSelectors, authActions } from "../../../store";

export function Login () {
    var authenticated = useSelector(authSelectors.getIsAuthenticated);
    var loading = useSelector(authSelectors.selectLoadingLogin);
    var error = useSelector(authSelectors.selectLoginError);
    var dispatch = useDispatch();

    var {
        value: email,
        bind: bindEmail
    } = useInput("");

    var {
        value: password,
        bind: bindPassword
    } = useInput("");

    if (authenticated) {
        return (<Redirect to="/" />);
    }

    return (
        <Unauthenticated>
            <Container>
                <Row className="justify-content-center">
                    <Col xs={6} >
                        {
                            (loading)
                                ? (<p>Logging you in...</p>)
                                : (null)
                        }
                        {
                            (error)
                                ? (<Alert variant="danger">{ error }</Alert>)
                                : (null)
                        }
                        <Form className="form-signin">
                            <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                            <Form.Group>
                                <label for="inputEmail" className="sr-only">Email address</label>
                                <input
                                    { ...bindEmail }
                                    type="email"
                                    id="inputEmail"
                                    className="form-control"
                                    placeholder="Email address"
                                    required
                                    autofocus
                                />
                            </Form.Group>

                            <Form.Group>
                                <label for="inputPassword" className="sr-only">Password</label>
                                <input
                                    { ...bindPassword }
                                    type="password"
                                    id="inputPassword"
                                    className="form-control"
                                    placeholder="Password"
                                    required
                                />
                            </Form.Group>

                            <div className="checkbox mb-3">
                                <label>
                                    <input type="checkbox" value="remember-me" />
                                    Remember me
                                </label>
                            </div>

                            <button
                                onClick={ login }
                                className="btn btn-lg btn-primary btn-block"
                                type="button"
                            >Sign in</button>

                            <p className="mt-5 mb-3 text-muted">&copy; 2017-2020</p>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </Unauthenticated>
    );

    function login () {
        dispatch(authActions.loginRequest({
            email,
            password
        }));
    }
}
