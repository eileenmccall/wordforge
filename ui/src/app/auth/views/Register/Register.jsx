import React from "react";
import "./Register.scss";
import { Button } from "react-bootstrap";
import { useDispatch } from "react-redux/dist/react-redux";
import { registerRequest } from "../../../store/auth/auth.actions";

export function Register () {
    var dispatch = useDispatch();

    return (
        <div>
            <h1>Register</h1>
            <Button variant="primary" onClick={handleButtonClick}>Click to register</Button>
        </div>
    );

    function handleButtonClick () {
        dispatch(registerRequest("eileen@wordforge.io"));
    }
}
