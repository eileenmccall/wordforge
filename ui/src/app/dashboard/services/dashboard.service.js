import { http } from "../../shared";
import { take } from "rxjs/operators";

export function getWords$ () {
    return http.get$("http://localhost:8080/api/words").pipe(
        take(1)
    );
}

export function createWord$ (word) {
    return http.post$("http://localhost:8080/api/words", word).pipe(
        take(1)
    );
}