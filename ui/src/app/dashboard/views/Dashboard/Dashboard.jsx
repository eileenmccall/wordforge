import React from "react";
import { Authenticated } from "../../../layout";
import "./Dashboard.scss";
import { Forges } from "./Forges/Forges";

export function Dashboard () {
    return (
        <Authenticated>
            <h1>Dashboard</h1>

            <hr />

            <Forges />

            <hr />

            <section>
                <h3>Collections</h3>
            </section>
        </Authenticated>
    );
}