import React, { useState, useEffect } from "react";
import AddForgeModal from "./AddForgeModal/AddForgeModal";
import { useSelector, useDispatch } from "react-redux/dist/react-redux";
import { Button, Spinner, Row, Col, Alert } from "react-bootstrap";
import Forge from "./Forge/Forge";
import "./Forges.scss";
import { forgesActions, forgesSelectors, authSelectors } from "../../../../store";
import { deleteForgeRequest, loadForges } from "../../../../store/forges/forges.actions";
import { DeleteForgeModal } from "./DeleteForgeModal/DeleteForgeModal";
import { Redirect } from "react-router-dom";

export function Forges () {
    var dispatch = useDispatch();

    var activeForgeID = useSelector(authSelectors.selectUserForgeID) || {};

    var { forges, loading, error } = useSelector(forgesSelectors.getForgesState);

    var [ showAddForgeModal, setShowAddForgeModal ] = useState(false);
    var [ showDeleteForgeModal, setShowDeleteForgeModal ] = useState(false);
    var [ forgeToDelete, setForgeToDelete ] = useState(null);

    var [ forgeDetailsID, setForgeDetailsID ] = useState(null);

    useEffect(function () {
        dispatch(loadForges());
    }, [ dispatch ]);

    if (loading) {
        return (
            <div className="loadingForgesSpinner">
                <h4 className="mb-4">Loading forges...</h4>
                <Spinner animation="border" variant="secondary" />
            </div>
        );
    }

    if (forgeDetailsID) {
        return (<Redirect to={`/forge-details/${forgeDetailsID}`} />);
    }

    return (
        <section>
            <AddForgeModal
                show={ showAddForgeModal }
                onSave={ handleModalSave }
                onHide={() => setShowAddForgeModal(false)}
            />

            <DeleteForgeModal
                show={ showDeleteForgeModal }
                forgeName={ forgeToDelete?.name }
                onConfirm={ handleDeleteForgeConfirm }
                onHide={ handleDeleteForgeCancel }
            />

            <div className="mb-3 d-flex justify-content-between">
                <h3>Forges</h3>
                <Button variant="primary" onClick={() => setShowAddForgeModal(true)}>Create Forge</Button>
            </div>
            {
                (error)
                ? (<Alert variant="danger">{ error }</Alert>)
                : (null)
            }

            <Row>
                {
                    (forges.length > 0)
                        ? (forges.map(function (forge) {
                            return (
                                <Col className="mb-4" md={6} lg={4} xl={3} key={forge._id}>
                                    <Forge
                                        active={activeForgeID == forge._id}
                                        forge={forge}
                                        onSwitchForge={getSwitchForgeHandler(forge._id)}
                                        onDelete={function () {
                                            return handleDeleteForge(forge);
                                        }}
                                        onDetails={function () {
                                            setForgeDetailsID(forge._id);
                                        }}
                                    />
                                </Col>
                            );
                        }))
                        : (<p>No forges!</p>)
                }
            </Row>
        </section>
    );

    function handleModalSave (formState) {
        setShowAddForgeModal(false);
        dispatch(forgesActions.addForgeRequest(formState));
    }

    function getSwitchForgeHandler (forgeID) {
        return function () {
            dispatch(forgesActions.switchActiveForgeRequest(forgeID));
        };
    }

    function handleDeleteForge (forge) {
        setForgeToDelete(forge);
        setShowDeleteForgeModal(true);
    }

    function handleDeleteForgeCancel () {
        setForgeToDelete(null);
        setShowDeleteForgeModal(false);
    }

    function handleDeleteForgeConfirm () {
        setForgeToDelete(null);
        setShowDeleteForgeModal(false);
        dispatch(deleteForgeRequest(forgeToDelete._id));
    }
}
