import React from "react";
import { Card, Button, Dropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight, faEllipsisH } from "@fortawesome/free-solid-svg-icons";
import "./Forge.scss";

export default function Forge ({
    forge: {
        name,
        description,
        sourceLanguage,
        targetLanguage,
        imageURL,
    } = {},
    active = false,
    onSwitchForge = () => {},
    onDelete = () => {},
    onDetails = () => {}
} = {}) {
    return (
        <Card>
            <Card.Header className="d-flex justify-content-between">
                { name }
                <Dropdown alignRight>
                    <Dropdown.Toggle className="ellipsis-dropdown">
                        <FontAwesomeIcon icon={faEllipsisH} />
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item onClick={onDetails}>View Details</Dropdown.Item>
                        <Dropdown.Item className="warning" onClick={onDelete}>Delete Forge</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </Card.Header>
            <Card.Img variant="top" src={ imageURL } />

            <Card.Body>
                <Card.Title>{ name }</Card.Title>

                <Card.Subtitle className="mb-2 text-muted">
                    { sourceLanguage.code.toUpperCase() } <FontAwesomeIcon icon={ faArrowRight } /> { targetLanguage.code.toUpperCase() }
                </Card.Subtitle>

                <Card.Text>{ description }</Card.Text>

                {
                    (active)
                    ? (<Button variant="light" disabled block>Active</Button>)
                    : (
                        <Button
                            variant="primary"
                            onClick={ onSwitchForge }
                            block
                        >
                            Switch Active Forge
                        </Button>
                    )
                }

            </Card.Body>
        </Card>
    );
}
