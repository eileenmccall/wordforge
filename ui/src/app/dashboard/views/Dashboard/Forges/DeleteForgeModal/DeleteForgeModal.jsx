import React from "react";
import { Modal, Button } from "react-bootstrap";

export function DeleteForgeModal ({
    show = false,
    forgeName = "forge",
    onConfirm = () => {},
    onHide = () => {}
}) {
    return (
        <Modal show={show} onHide={onHide}>
            <Modal.Header>
                Delete { forgeName }?
            </Modal.Header>

            <Modal.Body>
                Deleting a forge will also delete all words, sentences, and exercises associated with it.
                <br /><br />
                <b>This action is permanent and cannot be undone.</b>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={ onHide }>
                    Cancel
                </Button>

                <Button variant="danger" onClick={ onConfirm }>
                    Delete Forge
                </Button>
            </Modal.Footer>
        </Modal>
    );
}