import React from "react";
import { Modal, Form, Col, Row, Button } from "react-bootstrap";
import { useInput } from "../../../../../shared";
import { useSelector } from "react-redux/dist/react-redux";
import { appSelectors } from "../../../../../store";

export default function AddForgeModal ({
    show = false,
    onSave = () => {},
    onHide = () => {}
}) {
    var languages = useSelector(appSelectors.selectLanguages);
    var { value: name, bind: bindName, reset: resetName } = useInput("");
    var { value: description, bind: bindDescription, reset: resetDescription } = useInput("");
    var { value: sourceLanguage, bind: bindSourceLanguage, reset: resetSourceLanguage } = useInput("");
    var { value: targetLanguage, bind: bindTargetLanguage, reset: resetTargetLanguage } = useInput("");

    return (
        <Modal show={show} onHide={ handleHideclick } size="lg">
            <Modal.Header closeButton className="modalHeader">
                <Modal.Title>Add Forge</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Row>
                    <Col xs={12}>
                        <Form.Group>
                            <Form.Label htmlFor="name">Name</Form.Label>
                            <Form.Control type="text" name="name" { ...bindName } />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label htmlFor="description">Description</Form.Label>
                            <Form.Control type="text" name="description" { ...bindDescription } />
                        </Form.Group>

                        <Form.Group controlId="sourceLanguage">
                            <Form.Label>Source Language</Form.Label>
                            <Form.Control as="select" { ...bindSourceLanguage }>
                            <option></option>
                            {
                                languages.map(function buildSelect ({ _id, name: languageName, code }) {
                                    return (<option key={_id} value={code}>{languageName}</option>);
                                })
                            }
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId="targetLanguage">
                            <Form.Label>Target Language</Form.Label>
                            <Form.Control as="select" { ...bindTargetLanguage }>
                            <option></option>
                            {
                                languages.map(function buildSelect ({ _id, name: languageName, code }) {
                                    return (<option key={_id} value={code}>{languageName}</option>);
                                })
                            }
                            </Form.Control>
                        </Form.Group>
                    </Col>
                </Row>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={ handleHideclick }>
                    Close
                </Button>

                <Button variant="primary" onClick={ handleSaveClick }>
                    Save Forge
                </Button>
            </Modal.Footer>
        </Modal>
    );

    // function TargetLanguageSelect () {

    // }

    function handleSaveClick () {
        var formState = {
            name,
            description,
            sourceLanguage,
            targetLanguage,
            imageURL: "http://placehold.it/280x170"
        };

        onSave(formState);
        resetFormState();
    }

    function handleHideclick () {
        onHide();
        resetFormState();
    }

    function resetFormState () {
        resetName();
        resetDescription();
        resetTargetLanguage();
        resetSourceLanguage();
    }
}
