import React from "react";
import { Authenticated } from "../../../layout";
import { useSelector } from "react-redux/dist/react-redux";
import { forgesSelectors } from "../../../store";
import { useParams } from "react-router-dom";

export function ForgeDetails () {
    var { forgeID } = useParams();
    var forge = useSelector(forgesSelectors.getSelectForgeByIDSelector(forgeID));

    return (
        <Authenticated>
            <h1>{ forge.name }</h1>
        </Authenticated>
    );
}