import React from "react";
import { useSelector } from "react-redux/dist/react-redux";
import { appSelectors } from "../../../store";
import { Form } from "react-bootstrap";
import { useState } from "react";

export function WordInfoForm  ({
    formState: {
        wordRoot = "",
        wordTranslation = "",
        wordPartOfSpeech = ""
    } = {},
    formState,
    setFormState
} = {}) {
    var partsOfSpeech = useSelector(appSelectors.selectPartsOfSpeech);

    var [ root, setRoot ] = useState(wordRoot);
    var [ translation, setTranslation ] = useState(wordTranslation);
    var [ partOfSpeech, setPartOfSpeech ] = useState(wordPartOfSpeech);

    return (
        <>
            <h3>Word Details</h3>
            <Form>
                <Form.Group>
                    <Form.Label htmlFor="root">Root</Form.Label>
                    <Form.Control type="text" name="root" value={ root } onChange={ getChangeHandler(setRoot) }/>
                </Form.Group>

                <Form.Group>
                    <Form.Label htmlFor="translation">Translation</Form.Label>
                    <Form.Control type="text" name="translation" value={ translation} onChange={ getChangeHandler(setTranslation) } />
                </Form.Group>

                <Form.Group controlId="exampleForm.ControlSelect2">
                    <Form.Label>Part of speech</Form.Label>
                    <Form.Control as="select" value={ partOfSpeech } onChange={ getChangeHandler(setPartOfSpeech) }>
                    <option></option>
                    {
                        partsOfSpeech.map(function buildSelect ({ _id, name }) {
                            return (<option key={_id} value={_id}>{name}</option>);
                        })
                    }
                    </Form.Control>
                </Form.Group>
            </Form>
        </>
    );

    function getChangeHandler (stateUpdateFn) {
        return function changeHandler (event) {
            stateUpdateFn(event.target.value);

            var state = {
                ...formState,
                [event.target.name]: event.target.value
            };
            setFormState(state);
        };
    }
}