import React from "react";
import { Card, Table, Collapse, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";

export function ExercisesTable () {
    var [ open, setOpen ] = useState(false);
    return (
        <div>
            <h3>Exercises</h3>

            <Card>
                <Card.Header>
                    Translation
                    <button
                        className="navigation__nav__hamburger__button"
                        type="button"
                        onClick={ handleButtonClick }
                        aria-expanded={open}
                    >
                        <FontAwesomeIcon icon={faBars} />
                    </button>
                </Card.Header>

                <Collapse in={open}>
                    {/* Div necessary for smooth animation */}
                    <div>
                        <Table striped hover>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Hint</th>
                                    <th>Answer(s)</th>
                                <   th>Username</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><Form.Check aria-label="option 1" /></td>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                </tr>
                                <tr>
                                    <td><Form.Check aria-label="option 2" /></td>
                                    <td>Jacob</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                </tr>
                                    <tr>
                                    <td>3</td>
                                    <td colSpan="2">Larry the Bird</td>
                                    <td>@twitter</td>
                                </tr>
                            </tbody>
                        </Table>
                    </div>
                </Collapse>
            </Card>
        </div>
    );

    function handleButtonClick () {
        setOpen(!open);
    }
}