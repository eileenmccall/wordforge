import React, { useState, useEffect } from "react";
import "./LinkWordInput.scss";

export default function LinkWordInput () {

    var input = React.createRef();
    var [ text, setText ] = useState("");
    var [ focused, setFocused ] = useState(false);

    useEffect(function setInputHTML () {
        console.log("effect ran");
        if (input.current) {
            input.current.innerHTML = text;
        }
    }, [ input, text ]);

    return (
        <div
            className={
                "input " +
                // eslint-disable-next-line
                ((focused) ? "input--focused" : "")
            }
            ref={input}
            tabIndex={0}
            onFocus={handleFocus}
            onBlur={handleBlur}
            onMouseUp={handleMouseUp}
            onKeyUp={handleKeyUp}
        />
    );

    function handleFocus () {
        setFocused(true);
    }

    function handleBlur () {
        setFocused(false);
    }

    function handleMouseUp (event) {
        console.log(event);
    }

    function handleKeyUp (event) {
        // backspace
        if (event.keyCode == 8) {
            setText(text.slice(0, -1));
        } else {
            setText(`${text}${event.key}`);
        }
    }
}
