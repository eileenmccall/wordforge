import React from "react";
import { Modal, Form, Button } from "react-bootstrap";
import "./AddSentenceModal.scss";
import { useInput } from "../../../shared";


export default function AddSentenceModal ({ show, onHide, onSave }) {

    var {
        value: sentence,
        bind: bindSentence,
        reset: resetSentence
    } = useInput("");

    var {
        value: translation,
        bind: bindTranslation,
        reset: resetTranslation
    } = useInput("");

    function handleButtonClick () {
        resetSentence();
        resetTranslation();
        onSave({
            _id: Math.floor(Math.random() * 10000000),
            raw: sentence,
            translation,
            numberOfExercises: 0,
            numberOfWords: 0,
            reviewPercentage: 0
        });
        onHide();
    }

    return (
        <Modal show={show} onHide={onHide} size="lg">
            <Modal.Header closeButton>
                <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group>
                        <Form.Label htmlFor="sentence">Sentence</Form.Label>
                        <Form.Control type="text" name="sentence" { ...bindSentence } />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="translation">Translation</Form.Label>
                        <Form.Control type="text" name="translation" { ...bindTranslation } />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onHide}>
                    Close
                </Button>
                <Button variant="primary" onClick={handleButtonClick}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    );
}
