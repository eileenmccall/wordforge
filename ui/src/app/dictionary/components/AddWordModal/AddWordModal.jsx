import React from "react";
import { Modal, Button, Nav, Col, Row } from "react-bootstrap";
import { WordInfoForm } from "../WordInfoForm/WordInfoForm";
import { useState } from "react";
import { ExercisesTable } from "../ExercisesTable/ExercisesTable";
import "./AddWordModal.scss";
import { useDispatch } from "react-redux/dist/react-redux";
import { wordsActions } from "../../../store";

export default function AddWordModalComponent ({ show, onHide, onSave }) {

    var [ activeNav, setActiveNav ] = useState("info");
    var [ formState, setFormState ] = useState({});
    var [ generateTimer, setGenerateTimer ] = useState(null);
    var dispatch = useDispatch();

    return (
        <Modal show={show} onHide={onHide} size="lg">
            <Modal.Header closeButton className="modalHeader">
                <Modal.Title>Add Word</Modal.Title>
            </Modal.Header>

            <Nav className="navTabs" variant="tabs" activeKey={ activeNav } onSelect={ setActiveNav }>
                <Nav.Item>
                    <Nav.Link eventKey="info">Word Info</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link disabled eventKey="forms">Word Forms</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="exercises">Exercises (3)</Nav.Link>
                </Nav.Item>
            </Nav>

            <Modal.Body>
                <Row>
                    <Col xs={12}>
                        { getActiveTabComponent() }
                    </Col>
                </Row>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={onHide}>
                    Close
                </Button>

                <Button variant="primary" onClick={handleButtonClick}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    );

    function getActiveTabComponent () {
        switch (activeNav) {
            case "info":
                return (<WordInfoForm formState={ formState } setFormState={ formUpdated }/>);

            case "exercises":
                return (<ExercisesTable />);

            default:
                return null;
        }
    }

    function formUpdated (value) {
        setFormState(value);

        clearTimeout(generateTimer);
        setGenerateTimer(setTimeout(function generateExercises () {
            dispatch(wordsActions.generateWordExercises(formState));
        }, 3000));
    }

    function handleButtonClick () {
        onSave({
            _id: Math.floor(Math.random() * 1000000),
            root: formState.root,
            translation: formState.translation,
            partOfSpeech: {
                _id: "hello",
                name: "noun"
            },
            numberOfExercises: 0,
            numberOfSentences: 0,
            reviewPercentage: 0
        });
        onHide();
    }
}
