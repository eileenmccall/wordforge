import React from "react";
import "./Sentence.scss";

export default function Sentence ({
    html = "",
    translation = ""
}) {
    return (
        <div>
            <p className="sentence" dangerouslySetInnerHTML={{ __html: html }}></p>
            <p className="translation">{translation}</p>
        </div>
    );
}
