import React from "react";
import Sentence from "./Sentence/Sentence";
import "./WordDetails.scss";
import { Authenticated } from "../../../layout/Auth/Auth";

var sentenceXML = "<sentence><text>Je vais à la </text><word id='1'>supermarché</word></sentence>";
var parser = new DOMParser();

var xml = parser.parseFromString(sentenceXML, "text/xml");
var sentence = xml.childNodes[0];
var childNodes = sentence.childNodes;

var html = Array.from(childNodes).map(function (node) {
    // TODO
    // eslint-disable-next-line
    if (node.nodeName == "text") {
        return `<span class="text">${node.childNodes[0].nodeValue}</span>`;
    // TODO
    // eslint-disable-next-line
    } else if (node.nodeName == "word") {
        return `<span class="word">${node.childNodes[0].nodeValue}</span>`;
    }
    return `${node}`;
}).join("");

export function WordDetails () {
    return (
        <Authenticated>
            <h1>Word Details</h1>
            <hr/>
            <h2>Sentences</h2>
            <Sentence translation="I go to the supermarket" html={html}/>
        </Authenticated>
    );
}
