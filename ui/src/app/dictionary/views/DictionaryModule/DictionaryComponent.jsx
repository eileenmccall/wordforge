import React, { useState } from "react";

import AddWordModal from "../../components/AddWordModal/AddWordModal";
import WordsTable from "./WordsTable/WordsTable";
import TableSelector from "./TableSelector/TableSelector";
import SentencesTable from "./SentencesTable/SentencesTable";
import AddSentenceModal from "../../components/AddSentenceModal/AddSentenceModal";
import { useDispatch } from "react-redux/dist/react-redux";
import { Authenticated } from "../../../layout";
import { wordsActions, sentencesActions } from "../../../store";

export function Dictionary () {
    var dispatch = useDispatch();

    var [ showAddWordModal, setShowAddWordModal ] = useState(false);
    var [ showAddSentenceModal, setShowAddSentenceModal ] = useState(false);
    var [ active, setActive ] = useState("words");

    return (
        <Authenticated>
            <AddWordModal show={showAddWordModal} onHide={handleShowHideModal} onSave={handleSaveWord}/>
            <AddSentenceModal show={showAddSentenceModal} onHide={handleShowHideModal} onSave={handleSaveSentence} />

            <div className="my-5">
                <header className="mb-5">
                    <h1>Dictionary</h1>
                    <p>The words and sentences that you've added to Wordforge go here!</p>
                </header>

                <div className="d-flex justify-content-between mb-3">
                    <TableSelector active={active} setActive={setActive}/>

                    <button
                        onClick={handleShowHideModal}
                        className="btn btn-primary"
                        type="button"
                    >
                        {
                            (active == "words") ?
                            ("Add Word") :
                            ("Add Sentence")
                        }
                    </button>
                </div>

                {
                    // TODO
                    // eslint-disable-next-line
                    (active == "sentences") ?
                        (<SentencesTable />) :
                        (<WordsTable />)
                }
            </div>
        </Authenticated>
    );

    function handleShowHideModal () {
        if (active == "words") {
            setShowAddWordModal(!showAddWordModal);
        } else if (active == "sentences") {
            setShowAddSentenceModal(!showAddSentenceModal);
        }
    }

    function handleSaveWord (word) {
        dispatch(wordsActions.addWordRequest(word));
    }

    function handleSaveSentence (sentence) {
        dispatch(sentencesActions.addSentenceRequest(sentence));
    }
}