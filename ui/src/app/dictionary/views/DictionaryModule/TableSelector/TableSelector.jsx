import React from "react";
import { Button } from "react-bootstrap";

export default function TableSelector ({ active, setActive }) {
    function setWordsActive () {
        setActive("words");
    }

    function setSentencesActive () {
        setActive("sentences");
    }

    return (
        <div className="selector">
            <Button
                className="mr-3"
                variant={(active == "words") ? "primary" : "outline-primary"}
                onClick={setWordsActive}
            >
                Words
            </Button>
            <Button
                variant={(active == "sentences") ? "primary" : "outline-primary"}
                onClick={setSentencesActive}
            >
                Sentences
            </Button>
        </div>
    );
}
