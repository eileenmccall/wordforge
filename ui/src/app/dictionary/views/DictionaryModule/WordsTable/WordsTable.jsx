import React, { useEffect, Fragment } from "react";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Pagination } from "../../../../shared";
import { wordsActions, wordsSelectors } from "../../../../store";
import { useSelector, useDispatch } from "react-redux/dist/react-redux";

function WordsTable () {
    var words = useSelector(wordsSelectors.getSortedWords);
    var { pageIndex, loaded, error } = useSelector(function (state) {
        return state.words;
    });

    var dispatch = useDispatch();

    useEffect(function () {
        if (!loaded && !error) {
            dispatch(wordsActions.getWords());
        }
    }, [ dispatch, error, loaded ]);

    var tableHeaders = [
        "Word",
        "Translation",
        "Part of Speech",
        "Exercises",
        "Sentences",
        "Review %"
    ];

    function buildTableHeaders () {
        return tableHeaders.map(
            function mapHeaderToElement (header) {
                return (<th key={header}>{header}</th>);
            }
        );
    }

    function handlePaginate (index) {
        dispatch(wordsActions.paginate(index - 1));
    }

    return (
        <Fragment>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        { buildTableHeaders() }
                    </tr>
                </thead>
                <tbody>
                    {
                        (words) ?
                        (words.map(function buildTableRows (word) {
                            return (
                                <tr key={word.root}>
                                    <td><Link to={`/dictionary/${word._id}`}>{word.root}</Link></td>
                                    <td>{word.translation}</td>
                                    <td>{word.partOfSpeech.name}</td>
                                    <td>{word.numberOfExercises}</td>
                                    <td>{word.numberOfSentences}</td>
                                    <td>{word.reviewPercentage}</td>
                                </tr>
                            );
                        })) :
                        (null)
                    }
                </tbody>
            </Table>
            <Pagination activePage={pageIndex + 1} setActivePage={handlePaginate} numberOfPages={4}/>
        </Fragment>
    );
}

export default WordsTable;