import React, { Fragment, useEffect } from "react";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Pagination } from "../../../../shared";
import { useSelector, useDispatch } from "react-redux/dist/react-redux";
import { sentencesActions, sentencesSelectors } from "../../../../store";

export default function SentencesTable () {

    var sentences = useSelector(sentencesSelectors.getSortedSentences);

    var { pageIndex } = useSelector(function getState (state) {
        return state.sentences;
    });

    var dispatch = useDispatch();

    useEffect(function () {
        dispatch(sentencesActions.loadSentences());
    }, [ dispatch ]);

    function handlePaginate (index) {
        dispatch(sentencesActions.paginateSentences(index - 1));
    }

    var tableHeaders = [
        "Sentence",
        "Translation",
        "Exercises",
        "Words",
        "Review %"
    ];

    return (
        <Fragment>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        { buildTableHeaders() }
                    </tr>
                </thead>
                <tbody>
                    {
                        (sentences) ?
                        (sentences.map(function buildTableRows (sentence) {
                            return (
                                <tr key={sentence._id}>
                                    <td><Link to={`/dictionary/${sentence._id}`}>{sentence.raw}</Link></td>
                                    <td>{sentence.translation}</td>
                                    <td>{sentence.numberOfExercises}</td>
                                    <td>{sentence.numberOfWords}</td>
                                    <td>{sentence.reviewPercentage}</td>
                                </tr>
                            );
                        })) :
                        (null)
                    }
                </tbody>
            </Table>
            <Pagination activePage={pageIndex + 1} setActivePage={handlePaginate} numberOfPages={4}/>
        </Fragment>
    );

    function buildTableHeaders () {
        return tableHeaders.map(
            function mapHeaderToElement (header) {
                return (<th key={header}>{header}</th>);
            }
        );
    }
}
