import React from "react";
import { Badge } from "react-bootstrap";
import { InfoTooltip } from "../../../shared";

import "./ExerciseBadges.scss";

export default function ExerciseBadges ({ exercise } = {}) {
    var badges = [];

    return (
        <div className="badges">
            { getBadge(exercise) }&nbsp;
            {
                (badges.length > 0)
                    ?(<InfoTooltip message="Here's some badges"/>)
                    : (null)
            }
        </div>
    );

    function getBadge ( source ) {
        var { isNew, isFlagged, isLeech } = source;

        if (isNew) {
            badges.push(<Badge key="new" className="badges__badge" variant="success">NEW</Badge>);
        }

        if (isFlagged) {
            badges.push(<Badge key="flagged" className="badges__badge"  variant="danger">FLAGGED</Badge>);
        }

        if (isLeech) {
            badges.push(<Badge key="leech" className="badges__badge"  variant="warning">LEECH</Badge>);
        }

        return badges;
    }
}
