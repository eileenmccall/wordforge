import React from "react";
import "./RadioButton.scss";

export function RadioButton ({ children, fieldName, checked, onSelect = () => {} }) {
    return (
        <li
            className={[
                "review-options__option",
                // eslint-disable-next-line
                (checked ? "review-options__option--selected" : "")
            ].join(" ")}
        >
            <input
                type="radio"
                className="sr-only"
                id={ fieldName }
                value={ fieldName }
                checked={ checked }
                onChange={ clickHandler }
            />
            <label
                onClick={ clickHandler }
                htmlFor={ fieldName }
            >
                { children }
            </label>
        </li>
    );

    function clickHandler () {
        onSelect(fieldName);
    }
}
