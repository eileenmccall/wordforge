import React, { useState } from "react";
import "./AnswerTextarea.scss";

export function AnswerTextarea ({
    value,
    onChange = () => {},
    onKeyDown = () => {},
    placeholder = "Write your answer here",
    disabled
}) {
    var [ focused, setFocused ] = useState(false);

    return (
        <textarea
                placeholder={ placeholder }
                disabled={ disabled }
                className={[
                    "answerTextarea",
                    getFocusedClass()
                ].join(" ")}
                value={ value }
                onFocus={ handleFocus(true) }
                onBlur={ handleFocus(false) }
                onKeyDown={ onKeyDown }
                onChange={ onChange }
            ></textarea>
    );

    function getFocusedClass () {
        return (focused)
            ? ("answerTextarea--focused")
            : ("");
    }

    function handleFocus (isFocused) {
        return function updateFocused () {
            return setFocused(isFocused);
        };
    }
}
