import React from "react";
import "./TopBar.scss";

export default function TopBar ({ children }) {
    return (
        <header className="topBar">
            { children }
        </header>
    );
}
