import React, { Fragment } from "react";
import "./ReviewResults.scss";
import { Container } from "react-bootstrap";
import TopBar from "../../components/TopBar/TopBar";
import ItemsReviewedTable from "./ItemsReviewedTable/ItemsReviewedTable";
import ExercisesReviewedTable from "./ExercisesReviewedTable/ExercisesReviewedTable";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux/dist/react-redux";
import { sessionSelectors } from "../../../store";

export function ReviewResults () {
    var {
        exercises = [],
        reviewItems = [],
        duration
    } = useSelector(sessionSelectors.selectSessionResults) || {};

    return (
        <Fragment>
            <TopBar>
                <Container>
                    <div className="buttonContainer">
                        <button type="button" className="btn btn-success">
                            <Link to="/">Continue</Link></button>
                    </div>
                </Container>
            </TopBar>
            <Container>
                <section>
                    <h1>Review Complete!</h1>
                    <p>Nice job! Check out the tables below to see how you did!</p>

                    <h2>Stats:</h2>
                    <p><span>Duration: </span>{ duration }</p>

                    <h2>Items Reviewed:</h2>
                    <ItemsReviewedTable reviewItems={ reviewItems } />

                    <hr />

                    <h2>Exercises Reviewed:</h2>
                    <ExercisesReviewedTable exercises={ exercises } />
                </section>
            </Container>
        </Fragment>
    );
}
