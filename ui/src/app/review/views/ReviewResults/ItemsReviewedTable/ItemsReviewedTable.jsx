import React from "react";
import { Table } from "react-bootstrap";
import "./ItemsReviewedTable.scss";

export default function ItemsReviewedTable ({ reviewItems }) {
    var tableHeaders = [
        "Review Item",
        "Total Exercises",
        "# answered correctly",
        "# answered incorrectly",
        "% Correct",
        "% change",
    ];

    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    { tableHeaders.map(HeaderRow) }
                </tr>
            </thead>
            <tbody>
                { reviewItems.map(ItemReviewedRow) }
            </tbody>
        </ Table>
    );
}

function HeaderRow (header) {
    return (<th>{ header }</th>);
}

function ItemReviewedRow ({
    item,
    totalExercises,
    numberCorrect,
    numberIncorrect,
    correctPercent,
    totalCorrectPercent
}) {
    return (
        <tr>
            <td>{item}</td>
            <td>{totalExercises}</td>
            <td>{numberCorrect}</td>
            <td>{numberIncorrect}</td>
            <td>{correctPercent}</td>
            <td>{totalCorrectPercent}</td>
        </tr>
    );
}
