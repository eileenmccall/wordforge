import React from "react";
import "./ExercisesReviewedTable.scss";
import { Table, Button } from "react-bootstrap";
import { InfoTooltip } from "../../../../shared";

export default function ExercisesReviewedTable ({ exercises }) {
    var tableHeaders = [
        "Exercise Type",
        "Your Answer",
        "Correct Answer",
        "Times Reviewed",
        "Total Correct %"
    ];

    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    { tableHeaders.map(TableHeader) }
                    <th>
                        Mark for Review
                        <InfoTooltip message="Mark a difficult exercise to see it more often"/>
                        <span className="sr-only">
                            Mark a difficult exercise to see it more often
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
                { exercises.map(TableRow) }
            </tbody>
        </Table>
    );

    function TableHeader (header) {
        return (<th>{ header }</th>);
    }

    function TableRow ({
        type,
        userAnswer,
        correctAnswer,
        timesReviewed,
        correctPercent,
        marked
    }) {
        return (
            <tr>
                <td>{ type }</td>
                <td>{ userAnswer }</td>
                <td>{ correctAnswer }</td>
                <td>{ timesReviewed }</td>
                <td>{ correctPercent }</td>
                <td>{
                    (!marked)
                        ? (<Button type="button" className="btn btn-primary">Mark</Button>)
                        : (null)
                    }
                </td>
            </tr>
        );
    }
}
