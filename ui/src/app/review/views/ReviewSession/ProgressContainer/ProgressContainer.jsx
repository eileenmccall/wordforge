import React, { Fragment } from "react";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ProgressBar from "./ProgressBar/ProgressBar";
import "./ProgressContainer.scss";
import { useSelector } from "react-redux/dist/react-redux";
import { sessionSelectors } from "../../../../store";
import { useLocation, useParams } from "react-router-dom";

export default function ProgressContainer ({
    onExit = () => {}
} = {}) {
    var {
        currentExerciseIndex: index,
        exercises = []
    } = useSelector(sessionSelectors.selectSessionState) || {};

    console.log(useLocation());
    console.log(useParams());

    return (
        <Fragment>
            <div className="progressMeta">
                <span className="progressMeta__text">{ index + 1 } of { exercises.length }</span>
            </div>

            <div className="progressContainer">
                <FontAwesomeIcon icon={ faTimes } onClick={ onExit } className="progressContainer__icon"/>
                <div className="progressContainer__bar" >
                    <ProgressBar progressPercent={ calcProgressPercent(index, exercises.length) }/>
                </div>
            </div>
        </Fragment>
    );

    function calcProgressPercent (num, denom) {
        return Math.floor(num / denom * 100);
    }
}
