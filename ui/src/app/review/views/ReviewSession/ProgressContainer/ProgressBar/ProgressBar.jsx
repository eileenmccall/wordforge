import React from "react";
import "./ProgressBar.scss";

export default function ProgressBar ({
    progressPercent = 25
}) {

    return (
        <div className="progressBar">
            <div style={{ width: `${progressPercent}%` }} className="progressBar__progress" />
        </div>
    );
}
