import React from "react";
import "./BottomBar.scss";
import { Container } from "react-bootstrap";
import { CorrectAnswer } from "./CorrectAnswer/CorrectAnswer";
import { IncorrectAnswer } from "./IncorrectAnswer/IncorrectAnswer";
import { useDispatch, useSelector } from "react-redux/dist/react-redux";
import { sessionActions, sessionSelectors } from "../../../../store";

export default function BottomBar ({
    onRate = () => {}
}) {
    var dispatch = useDispatch();
    var answer = useSelector(sessionSelectors.selectCurrentAnswer) || {};

    /*
        id of the current exercise
        whether it was answered correctly or incorrectly
    */

    return (
        <div className={[
            "bottomBar",
            getCorrectnessClass()
        ].join(" ")}>
            <Container
                className="bottomBar__container">
                {
                    (answer.correct)
                        ? (<CorrectAnswer
                            answer={ answer }
                            onRateEasy={ handleRateEasy }
                            onRateNormal={ handleRateNormal }
                            onRateDifficult={ handleRateDifficult }
                        />)
                        : (<IncorrectAnswer
                            answer={ answer }
                        />)
                }
            </Container>
        </div>
    );

    function getCorrectnessClass () {
        return (answer.correct)
            ? ("bottomBar--correct")
            : ("bottomBar--incorrect");
    }

    function handleRateEasy () {
        dispatch(sessionActions.markExerciseEasy());
    }

    function handleRateNormal () {
        dispatch(sessionActions.markExerciseNormal());
    }

    function handleRateDifficult () {
        dispatch(sessionActions.markExerciseDifficult());
    }
}
