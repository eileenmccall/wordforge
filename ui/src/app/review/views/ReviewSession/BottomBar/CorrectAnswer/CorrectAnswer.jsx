import React from "react";
import { InfoTooltip } from "../../../../../shared";
import "./CorrectAnswer.scss";

export function CorrectAnswer ({
    answer,
    easySchedule = "1d",
    normalSchedule = "1h",
    difficultSchedule = "10m",
    onRateEasy,
    onRateNormal,
    onRateDifficult
}) {
    return (
        <div>
            <div className="result">
                <h3>You answered correctly!</h3>
                <p>Your answer: { answer.answer }</p>
            </div>

            <div className="rating">
                <p className="rating__text">
                    How difficult was this exerise?&nbsp;
                    <InfoTooltip
                        message="Rating determines how soon you will see this exercise again."
                    />
                </p>

                <button
                    className="btn btn-success rating__easy"
                    onClick={ onRateEasy }
                >
                    Easy ({easySchedule})
                </button>

                <button
                    className="btn btn-light rating__normal"
                    onClick={ onRateNormal }
                >
                    Normal ({ normalSchedule })
                </button>

                <button
                    className="btn btn-danger rating__challenging"
                    onClick={ onRateDifficult }
                >
                    Challenging ({ difficultSchedule })
                </button>
            </div>
        </div>
    );
}
