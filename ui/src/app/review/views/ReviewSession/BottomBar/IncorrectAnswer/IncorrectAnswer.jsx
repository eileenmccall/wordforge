import React from "react";
import { Button } from "react-bootstrap";
import { useDispatch } from "react-redux/dist/react-redux";
import { sessionActions } from "../../../../../store";

export function IncorrectAnswer ({
    answer
}) {
    var dispatch = useDispatch();

    return (
        <div className="result">
            <h3>You answered incorrectly!</h3>
            <p>Your answer: { answer.answer }</p>

            <Button
                variant="primary"
                onClick={ nextExercise }
            >
                Continue
            </Button>

            <Button variant="secondary">Accept my answer</Button>
            <Button variant="danger">Flag Exercise</Button>
        </div>
    );

    function nextExercise () {
        dispatch(sessionActions.incorrectAnswerContinue());
    }
}
