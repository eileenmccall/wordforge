import React, { Fragment, useState } from "react";
import { Redirect, useParams } from "react-router-dom";
import "./ReviewSession.scss";
import { Container } from "react-bootstrap";
import ExerciseContainer from "./ExerciseContainer/ExerciseContainer";
import TopBar from "../../components/TopBar/TopBar";
import { useSelector } from "react-redux/dist/react-redux";
import { sessionSelectors } from "../../../store";
import ProgressContainer from "./ProgressContainer/ProgressContainer";
import BottomBar from "./BottomBar/BottomBar";

export function ReviewSession () {
    var [ exit, setExit ] = useState(false);
    var {
        reviewing,
        finished,
        answerSubmitted,
        exercises = [],
    } = useSelector(sessionSelectors.selectSessionState) || {};

    var { sessionId } = useParams();
    console.log(sessionId);

    if (finished) {
        return (<Redirect to="/review" />);
    }

    if (!reviewing) {
        return (<Redirect to="/review" />);
    }

    if (exit) {
        return (<Redirect />);
    }

    return (
        <Fragment>
            <TopBar>
                <Container>
                    <ProgressContainer onExit={ handleExitSession } />
                </Container>
            </TopBar>

            <div className="studyContainer">
                <div className="study">
                    <ExerciseContainer exercise={ exercises[0] }/>
                </div>
            </div>

            {
                (answerSubmitted)
                    ? (<BottomBar />)
                    : (null)
            }
        </Fragment>
    );

    function handleExitSession () {
        setExit(true);

    }
}
