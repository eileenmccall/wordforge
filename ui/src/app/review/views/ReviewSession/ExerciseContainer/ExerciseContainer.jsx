import React from "react";
import { TranslateExercise } from "./TranslateExercise/TranslateExercise";
import { useDispatch, useSelector } from "react-redux/dist/react-redux";
import { sessionSelectors, sessionActions } from "../../../../store";

import "./ExerciseContainer.scss";
import { PictureToWordExercise } from "./PictureToWordExercise/PictureToWordExercise";
import { WordToPictureExercise } from "./WordToPictureExercise/WordToPictureExercise";
import TranscribeAudioExercise from "./TranscribeAudioExercise/TranscribeAudioExercise";

export default function ExerciseContainer () {
    var dispatch = useDispatch();
    var { exercise, answerSubmitted } = useSelector(sessionSelectors.selectSessionState);

    var bindExercise = {
        key: exercise._id,
        exercise: exercise,
        onCorrectAnswer: handleCorrectAnswer,
        onIncorrectAnswer: handleIncorrectAnswer
    };

    return (
        <section className="exerciseContainer">
            { getExerciseComponent(exercise) }
        </section>
    );

    function getExerciseComponent (e) {
        switch (e.type) {
            case "translateToTarget":
                return (<TranslateExercise
                    { ...bindExercise }
                    answerSubmitted={ answerSubmitted }
                    exerciseText={ exercise.source }
                    translateToLanguage={ exercise.targetLanguage }
                />);

            case "translateToSource":
                return (<TranslateExercise
                    { ...bindExercise }
                    answerSubmitted={ answerSubmitted }
                    exerciseText={ exercise.target }
                    translateToLanguage={ exercise.sourceLanguage }
                />);

            case "pictureToWord":
                return (<PictureToWordExercise { ...bindExercise } />);

            case "wordToPicture":
                return (<WordToPictureExercise { ...bindExercise } />);

            case "transcribeAudio":
                return (<TranscribeAudioExercise { ...bindExercise } />);

            default:
                return null;
        }
    }

    function handleCorrectAnswer (answer) {
        dispatch(sessionActions.answerExerciseCorrectly(answer));
    }

    function handleIncorrectAnswer (answer) {
        dispatch(sessionActions.answerExerciseIncorrectly(answer));
    }
}
