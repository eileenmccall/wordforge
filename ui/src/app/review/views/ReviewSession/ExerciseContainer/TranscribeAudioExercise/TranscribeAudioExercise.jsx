import React, { useState } from "react";
import { AudioButton } from "../../../../../shared";
import { Button } from "react-bootstrap";
import { checkAnswer } from "../../../../../shared/utils";
import { AnswerTextarea } from "../../../../components/AnswerTextarea/AnswerTextarea";

export default function TranscribeAudioExercise ({
    exercise: {
        audioURL,
        acceptableAnswers = []
    } = {},
    onCorrectAnswer = () => {},
    onIncorrectAnswer = () => {}
} = {}) {
    var [ inputDisabled, setInputDisabled ] = useState(false);
    var [ buttonDisabled, setButtonDisabled ] = useState(false);
    var [ inputValue, setInputValue ] = useState("");

    return (
        <div>
            <h2>Transcribe the audio that you hear</h2>
            <AudioButton
                autoplayThreshold={ 1 }
                url={ audioURL }
            />

            <AnswerTextarea
                value={ inputValue }
                disabled={ inputDisabled }
                onChange={ validate }
                onKeyDown={ handleKeyDown }
            />

            <Button
                disabled={ buttonDisabled }
                variant="primary"
            >Submit</Button>
        </div>
    );

    function validate (event) {
        setInputValue(event.target.value);
    }

    function handleKeyDown (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            handleSubmit();
        }
    }

    function handleSubmit () {
        setInputDisabled(true);
        setButtonDisabled(true);

        if (checkAnswer(acceptableAnswers, inputValue)) {
            onCorrectAnswer(inputValue);
        } else {
            onIncorrectAnswer(inputValue);
        }
    }
}
