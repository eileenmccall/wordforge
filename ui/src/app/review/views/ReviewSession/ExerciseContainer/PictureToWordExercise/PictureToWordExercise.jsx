import React from "react";
import "./PictureToWordExercise.scss";
import { InputGroup, FormControl, Button } from "react-bootstrap";
import { checkAnswer } from "../../../../../shared/utils/index";
import { useState } from "react";

export function PictureToWordExercise ({
    exercise: {
        imageURLs,
        acceptableAnswers
    } = {},
    onCorrectAnswer = () => {},
    onIncorrectAnswer = () => {}
}) {
    var [ inputValue, setInputValue ] = useState("");
    var [ inputDisabled, setInputDisabled ] = useState(false);
    var [ buttonDisabled, setbuttonDisabled ] = useState(true);

    return (
        <div>
            <h3>Write the word that goes with the picture(s)</h3>

            <div className="imageContainer">
                { images() }
            </div>

            <InputGroup className="answer">
                <FormControl
                    disabled={ inputDisabled }
                    value={ inputValue }
                    onChange={ validate }
                    placeholder="Write your answer here"
                    aria-label="Answer"
                />
            </InputGroup>

            <Button
                disabled={ buttonDisabled }
                variant="primary"
                onClick={ onSubmit }
            >Submit</Button>
        </div>
    );

    function images () {
        return imageURLs.map(function mapToDiv ({ _id, url }) {
            return (<div
                className="imageContainer__image"
                onHover
                key={ _id }
                style={{
                    backgroundImage: `url(${url})`
                }}
            />);
        });
    }

    function validate (event) {
        var value = event.target.value;
        setInputValue(value);
        if (value.length == 0) {
            setbuttonDisabled(true);
        } else {
            setbuttonDisabled(false);
        }
    }

    function onSubmit () {
        setbuttonDisabled(true);
        setInputDisabled(true);

        if (checkAnswer(acceptableAnswers, inputValue)) {
            onCorrectAnswer(inputValue);
        } else {
            onIncorrectAnswer(inputValue);
        }
    }
}
