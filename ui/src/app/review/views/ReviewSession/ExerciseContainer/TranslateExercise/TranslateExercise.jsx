import React, { Fragment, useState } from "react";
import "./TranslateExercise.scss";
import { AudioButton } from "../../../../../shared";
import ExerciseBadges from "../../../../components/ExerciseBadges/ExerciseBadges";
import { Button } from "react-bootstrap";
import { AnswerTextarea } from "../../../../components/AnswerTextarea/AnswerTextarea";
import { checkAnswer } from "../../../../../shared/utils";

export function TranslateExercise ({
    exercise: {
        audioURLs,
        acceptableAnswers = []
    } = {},
    exercise,
    translateToLanguage,
    exerciseText,
    onCorrectAnswer = () => {},
    onIncorrectAnswer = () => {},
    answerSubmitted = false
} = {}) {
    var [ inputDisabled, setInputDisabled ] = useState(false);
    var [ buttonDisabled, setbuttonDisabled ] = useState(true);
    var [ inputValue, setInputValue ] = useState("");

    return (
        <Fragment>
            <ExerciseBadges exercise={ exercise } />
            <h3 className="mb-3">Translate into { translateToLanguage }:</h3>

            <div className="d-flex align-items-center mb-3">
                {
                    (audioURLs.length > 0)
                        ? (<AudioButton url={ audioURLs[0] } />)
                        : (null)
                }
                <p className="source mb-0 ml-3">{ exerciseText }</p>
            </div>

            <AnswerTextarea
                value={ inputValue }
                onChange={ validate }
                disabled={ inputDisabled || answerSubmitted }
            />

            <div className="buttonContainer">
                <Button
                    type="button"
                    variant="primary"
                    onClick={ handleButtonClick }
                    disabled={ buttonDisabled || answerSubmitted }
                >Submit</Button>
            </div>
        </Fragment>
    );

    function validate (event) {
        var value = event.target.value;
        setInputValue(value);
        if (value.length == 0) {
            setbuttonDisabled(true);
        } else {
            setbuttonDisabled(false);
        }
    }

    function handleButtonClick () {
        setInputDisabled(false);
        setbuttonDisabled(false);

        if (checkAnswer(acceptableAnswers, inputValue)) {
            onCorrectAnswer(inputValue);
        } else {
            onIncorrectAnswer(inputValue);
        }
    }
}
