import React from "react";
import "./WordToPictureExercise.scss";
import { useState } from "react";

export function WordToPictureExercise ({
    exercise: {
        answerId,
        target,
        imageURLs = [],
    } = {},
    onCorrectAnswer = () => {},
    onIncorrectAnswer = () => {}
} = {}) {
    var [ selectedImage, setSelectedImage ] = useState(null);

    return (
        <div>
            <h3>Click on the image that goes with the word or phrase:</h3>

            <h4 className="word mb-5 mt-5 text-center">{ target }</h4>

            <div className="imageContainer">
                { images() }
            </div>
        </div>
    );

    function images () {
        return imageURLs.map(function mapToDiv ({ _id, url }) {
            return (
                <div
                    className={[
                        "imageContainer__image",
                        getCorrectClass(_id)

                    ].join(" ")}
                    key={ _id }
                    style={{
                        backgroundImage: `url(${url})`
                    }}
                    onClick={ handleImageClick(_id) }
                />
            );
        });
    }

    function getCorrectClass (id) {
        if (!selectedImage) {
            return null;
        }

        if (answerId == id) {
            return "imageContainer__image--correct";
        }

        if (selectedImage == id) {
            return "imageContainer__image--incorrect";
        }

        return null;
    }

    function handleImageClick (imageId) {
        return function clickHandler () {
            setSelectedImage(imageId);

            if (imageId == answerId) {
                onCorrectAnswer(imageId);
            } else {
                onIncorrectAnswer(imageId);
            }
        };
    }
}