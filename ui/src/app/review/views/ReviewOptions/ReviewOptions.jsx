import React, { useState } from "react";
import "./ReviewOptions.scss";
import { Authenticated } from "../../../layout/Auth/Auth";
import { useDispatch, useSelector } from "react-redux/dist/react-redux";
import { sessionActions, sessionSelectors } from "../../../store";
import { Redirect } from "react-router-dom";
import { Row, Col, InputGroup, Form, FormControl, Spinner } from "react-bootstrap";
import { getSelectHandler } from "../../../shared/utils";

export function ReviewOptions () {
    var dispatch = useDispatch();
    var {
        loading,
        sessionId
    } = useSelector(sessionSelectors.selectSessionState);

    var [ strategy, setStrategy ] = useState("new");
    var [ condition, setCondition ] = useState("duration");
    var [ duration, setDuration ] = useState("quick");
    var [ count, setCount ] = useState("value");
    var [ countValue, setCountValue ] = useState(null);

    if (sessionId) {
        return <Redirect to={`review/${sessionId}`} />;
    }

    if (loading) {
        return (
            <div className="spinnerContainer">
                <h4 className="mb-4">Loading review session...</h4>
                <Spinner animation="border" variant="secondary" />
            </div>
        );
    }

    return (
        <Authenticated>
            <div className="chooseExercises">
                <h1>New Review</h1>
                <Form>
                    <Form.Group>
                        <Form.Label>Which exercises would you like to review?</Form.Label>
                        <Form.Check
                            name="strategy"
                            type="radio"
                            id="new"
                            value="new"
                            label="New exercises only"
                            checked={ strategy == "new" }
                            onChange={ getSelectHandler(setStrategy) }
                        />
                        <Form.Check
                            name="strategy"
                            type="radio"
                            id="mostOverdue"
                            value="mostOverdue"
                            label="Most overdue exercises"
                            checked={ strategy == "mostOverdue" }
                            onChange={ getSelectHandler(setStrategy) }
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>How would you like to review?</Form.Label>
                        <Row className="mb-3">
                            <Col xs="6">
                                <InputGroup>
                                    <select value={ condition } onChange={ getSelectHandler(setCondition) } className="custom-select">
                                        <option name="value" value="duration">Review by duration</option>
                                        <option name="value" value="count">Review by # of Exercises</option>
                                    </select>
                                </InputGroup>
                            </Col>
                        </Row>

                        {
                            (condition == "duration")
                                ? (
                                    <>
                                        <Form.Check
                                            name="duration"
                                            type="radio"
                                            id="quick"
                                            value="quick"
                                            label="Quick (5m)"
                                            checked={ duration == "quick" }
                                            onChange={ getSelectHandler(setDuration) }
                                            />
                                        <Form.Check
                                            name="duration"
                                            type="radio"
                                            id="normal"
                                            value="normal"
                                            label="Normal (15m)"
                                            checked={ duration == "normal" }
                                            onChange={ getSelectHandler(setDuration) }
                                        />
                                        <Form.Check
                                            name="duration"
                                            type="radio"
                                            id="long"
                                            value="long"
                                            label="Long (1h)"
                                            checked={ duration == "long" }
                                            onChange={ getSelectHandler(setDuration) }
                                        />
                                        <Form.Check
                                            name="duration"
                                            type="radio"
                                            id="infinite"
                                            value="infinite"
                                            label="Infinite"
                                            checked={ duration == "infinite" }
                                            onChange={ getSelectHandler(setDuration) }
                                        />
                                    </>
                                )
                                : (
                                    <>
                                        <Form.Group>
                                            <Form.Check
                                                name="count"
                                                type="radio"
                                                id="value"
                                                value="value"
                                                label="Enter value"
                                                checked={ count == "value" }
                                                onChange={ getSelectHandler(setCount) }
                                            />
                                            <Form.Check
                                                name="count"
                                                type="radio"
                                                id="infinite"
                                                value="infinite"
                                                label="Infinite"
                                                checked={ count == "infinite" }
                                                onChange={ getSelectHandler(setCount) }
                                            />
                                        </Form.Group>
                                        <div>
                                            {
                                                (count == "value")
                                                    ? (
                                                        <InputGroup>
                                                            <FormControl
                                                                placeholder="# of exercises"
                                                                type="number"
                                                                onChange={ getSelectHandler(setCountValue) }
                                                            />
                                                        </InputGroup>
                                                    )
                                                    : (null)
                                            }
                                        </div>
                                    </>
                                )
                        }
                    </Form.Group>
                </Form>

                <div>
                    <button
                        className="btn btn-primary"
                        type="button"
                        onClick={ handleButtonClick }
                    >Start Review</button>
                </div>
            </div>
        </Authenticated>
    );

    function handleButtonClick () {
        dispatch(sessionActions.startSessionRequest(
            {
                strategy,
                condition,
                duration,
                count: (count == "value")
                    ? (countValue)
                    : ("infinite")
            }
        ));
    }
}
