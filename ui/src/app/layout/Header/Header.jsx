import React, { useState } from "react";
import { Container, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./Header.scss";
import { LogoIcon } from "../../shared";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { useDispatch } from "react-redux/dist/react-redux";
import { authActions } from "../../store";

export function Header () {
    var dispatch = useDispatch();
    var [ expanded, setExpanded ] = useState(false);

    return (
        <div className="bg">
            <Container>
                <nav className="navigation">
                    <div className="navigation__brand">
                        <span className="navigation__brand__logo" >
                            <LogoIcon fill="#fff" />
                        </span>
                        <span className="navigation__brand__text">
                            Wordforge
                        </span>
                    </div>

                    <div className={[
                        "navigation__nav",
                        (expanded)
                            ? ("navigation__nav--expanded")
                            : ("")
                    ].join(" ")}>
                        <div className="navigation__nav__hamburger">
                            <button
                                className="navigation__nav__hamburger__button"
                                type="button"
                                onClick={handleButtonClick}
                            >
                                <FontAwesomeIcon icon={faBars} />
                            </button>
                        </div>

                        <ol className="navigation__nav__list">
                            <li className="navigation__nav__list__item">
                                <Link to="/">
                                    Dashboard
                                </Link>
                            </li>

                            <li className="navigation__nav__list__item">
                                <Link to="/dictionary">
                                    Dictionary
                                </Link>
                            </li>

                            <li className="navigation__nav__list__item">
                                <Link to="/review">
                                    Review
                                </Link>
                            </li>

                            <li className="navigation__nav__list__item">
                                <Button variant="link" onClick={handleLinkClick}>
                                    Logout
                                </Button>
                            </li>
                        </ol>
                    </div>
                </nav>
            </Container>
        </div>
    );

    function handleButtonClick () {
        console.log(expanded);
        setExpanded(!expanded);
    }

    function handleLinkClick () {
        dispatch(authActions.logoutRequest());
    }
}
