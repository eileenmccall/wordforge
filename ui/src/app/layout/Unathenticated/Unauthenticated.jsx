import React from "react";
import "./Unauthenticated.scss";

export function Unauthenticated ({ children }) {
    return (
        <div>
            { children }
        </div>
    );
}
