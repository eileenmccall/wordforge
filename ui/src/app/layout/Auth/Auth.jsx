import React, { Fragment } from "react";
import { useSelector } from "react-redux/dist/react-redux";
import { authSelectors } from "../../store";
import "./Authenticated.scss";
import { Header } from "../Header/Header";
import { Container } from "react-bootstrap";
import { Redirect } from "react-router-dom";

export function Authenticated ({ children }) {
    var authenticated = useSelector(authSelectors.getIsAuthenticated);
    var loadingUser = useSelector(authSelectors.selectLoadingUser);

    if (!authenticated && !loadingUser) {
        return (
            <Redirect to="/login" />
        );
    }

    return (
        <Fragment>
            <Header />
            <Container className="main">
                { children }
            </Container>
        </Fragment>
    );
}
