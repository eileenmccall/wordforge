import React from "react";
import "./App.scss";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import { Register, Login } from "./auth";
import { Dashboard } from "./dashboard";
import { ReviewOptions, ReviewSession, ReviewResults } from "./review";
import { useDispatch } from "react-redux/dist/react-redux";
import { appActions, authActions } from "./store";
import { WordDetails, Dictionary } from "./dictionary";
import { ForgeDetails } from "./dashboard/views/ForgeDetails/ForgeDetails";
import { Toasts } from "./shared/components/Toasts/Toasts";

function App () {
    var dispatch = useDispatch();
    dispatch(appActions.loadPartsOfSpeechRequest());
    dispatch(appActions.getLanguagesRequest());
    dispatch(authActions.getUserRequest());

    return (
        <>
            <Toasts />
                <Router>
                    <Switch>
                        <Route path="/login">
                            <Login />
                        </Route>
                        <Route path="/register">
                            <Register />
                        </Route>

                        <Route path="/dictionary/:id">
                            <WordDetails />
                        </Route>
                        <Route path="/dictionary">
                            <Dictionary />
                        </Route>

                        <Route path="/review/:sessionId/results">
                            <ReviewResults />
                        </Route>
                        <Route path="/review/:sessionId">
                            <ReviewSession />
                        </Route>
                        <Route path="/review">
                            <ReviewOptions />
                        </Route>

                        <Route exact path="/">
                            <Dashboard />
                        </Route>

                        <Route path="/forge-details/:forgeID">
                            <ForgeDetails />
                        </Route>
                    </Switch>
                </Router>
        </>
    );
}

export default App;
