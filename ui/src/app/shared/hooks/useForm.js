import { useState, useEffect, useCallback } from "react";

export function useForm (stateSchema, validationSchema = {}, callback) {
    var [ state, setState ] = useState(stateSchema);
    var [ disable, setDisable ] = useState(true);
    var [ isDirty, setIsDirty ] = useState(false);

    // Disable button in initial render.
    useEffect(function effect () {
        setDisable(true);
    }, []);


    // Used to disable submit button if there's an error in state
    // or the required field in state has no value.
    // Wrapped in useCallback to cached the function to avoid intensive memory leaked
    // in every re-render in component
    var validateState = useCallback(function cb () {
        var hasErrorInState = Object.keys(validationSchema).some(function (key) {
            var isInputFieldRequired = validationSchema[key].required;
            var stateValue = state[key].value; // state value
            var stateError = state[key].error; // state error

            return (isInputFieldRequired && !stateValue) || stateError;
        });

        return hasErrorInState;
    }, [ state, validationSchema ]);

  // For every changed in our state this will be fired
    // To be able to disable the button
    useEffect(function effect () {
        if (isDirty) {
            setDisable(validateState());
        }
    }, [ state, isDirty, validateState ]);

    // Used to handle every changes in every input
    var handleOnChange = useCallback(
        function (event) {
            setIsDirty(true);

            const name = event.target.name;
            const value = event.target.value;

            let error = "";
            if (validationSchema[name].required) {
                if (!value) {
                    error = "This is required field.";
                }
            }

            if (
                validationSchema[name].validator !== null &&
                typeof validationSchema[name].validator === "object"
            ) {
                if (value && !validationSchema[name].validator.regEx.test(value)) {
                    error = validationSchema[name].validator.error;
                }
            }

            setState(function (prevState) {
                return {
                    ...prevState,
                    [name]: { value, error },
                    };
            });
        },
        [ validationSchema ]
    );

    const handleOnSubmit = useCallback(
        function (event) {
            event.preventDefault();

            // Make sure that validateState returns false
            // Before calling the submit callback function
            if (!validateState()) {
                callback(state);
            }
        },
        [ callback, state, validateState ]
    );

    return { state, disable, handleOnChange, handleOnSubmit };
}
