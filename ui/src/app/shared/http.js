import { Observable } from "rxjs";
import axios from "axios";
import { take } from "rxjs/operators";

export function get$ (
    path,
    config
) {
    return new Observable(function (observer) {
        axios.get(path, { ...config, withCredentials: true }).then(function handleResponse (response) {
            observer.next(response.data);
            observer.complete();
        })
        .catch(function handleError (error) {
            var { message } = error.response.data;
            observer.error(message);
        });
    }).pipe(take(1));
}

export function post$ (
    path,
    data,
    config
) {
    return new Observable(function (observer) {
        axios.post(path, data, { ...config, withCredentials: true }).then(function handleResponse (response) {
            observer.next(response.data);
            observer.complete();
        })
        .catch(function handleError (error) {
            var { message } = error.response.data;
            observer.error(message);
        });
    });
}

export function delete$ (
    path,
    config
) {
    return new Observable(function (observer) {
        axios.delete(path, { ...config, withCredentials: true }).then(function handleResponse (response) {
            observer.next(response.data);
            observer.complete();
        })
        .catch(function handleError (error) {
            var { message } = error.response.data;
            observer.error(message);
        });
    });
}

// function put$ (path: string, body: any) {
//     // TODO
// }

// function delete$ (path: string, body: any) {
//     // TODO
// }