import React from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";

export function InfoTooltip ({
    placement = "top",
    id = "infoTooltip",
    message = ""
}) {
    return (
        <OverlayTrigger
            placement={ placement }
            overlay={
                <Tooltip id={id}>
                    { message }
                </Tooltip>
            }
        >
            <FontAwesomeIcon icon={ faQuestionCircle } />
        </OverlayTrigger>
    );
}
