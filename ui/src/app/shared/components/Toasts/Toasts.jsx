import React from "react";
import { useSelector, useDispatch } from "react-redux/dist/react-redux";
import "./Toasts.scss";
import { Toast } from "react-bootstrap";
import { removeToast } from "../../../store/toasts/toasts.actions";

export function Toasts () {
    var { toasts } = useSelector((state) => state.toasts);
    var dispatch = useDispatch();

    return (
        <div className="toasts-container">
            {
                toasts.map(
                    function mapToBSToast ({ id, options }) {
                        var {
                            header = null,
                            autohide,
                            delay,
                            body
                        } = options;

                        return (
                            <Toast
                                className="toasts-container__toast"
                                autohide={ autohide || true }
                                delay={ delay || 5000 }
                                key={ id }
                                onClose={ getDismissToastFn(id) }
                            >
                                {
                                    (header)
                                        ? (<Toast.Header>
                                            {
                                                (header.image)
                                                    ? (
                                                        <img
                                                            src={ header.image.src }
                                                            className=""
                                                            alt={ header.image.alt || "" }
                                                        />
                                                    )
                                                    : (null)
                                            }

                                            <strong className="mr-auto">{ header.title } </strong>
                                            <small>Just now</small>
                                        </Toast.Header>)
                                        : (null)
                                }

                                <Toast.Body>
                                    { body || "" }
                                </Toast.Body>
                            </Toast>
                        );
                    }
                )
            }
        </div>
    );

    function getDismissToastFn (id) {
        return function () {
            return dispatch(removeToast(id));
        };
    }
}