import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faVolumeUp } from "@fortawesome/free-solid-svg-icons";
import Sound from "react-sound";
import { Button } from "react-bootstrap";

export function AudioButton ({ autoplayThreshold = 0, url = "" } = {}) {
    var [ audioHasPlayedTimes, setAudioHasPlayedTimes ] = useState(0);
    var [ position, setPosition ] = useState(0);
    var [ audioStatus, setAudioStatus ] = useState("STOPPED");

    useEffect(function () {
        if (
            autoplayThreshold > 0 &&
            audioHasPlayedTimes < autoplayThreshold
        ) {
            setAudioStatus("PLAYING");
            setAudioHasPlayedTimes(audioHasPlayedTimes + 1);
        }
    }, [ audioHasPlayedTimes, autoplayThreshold ]);

    return (
        <Button variant="primary" onClick={ playAudio }>
            <FontAwesomeIcon icon={ faVolumeUp } />
            <Sound
                url={ url }
                playStatus={ audioStatus }
                position={ position }
                onPlaying={ handlePlaying }
                autoLoad={true}
                onFinishedPlaying={handleFinishedPlaying}
            />
        </Button>
    );

    function playAudio () {
        setAudioStatus("PLAYING");
    }

    function handlePlaying ({ position: playbackPosition }) {
        setPosition(playbackPosition);
    }

    function handleFinishedPlaying () {
        setAudioStatus("STOPPED");
        setPosition(0);
    }
}
