import React from "react";
import { Pagination as BsPagination } from "react-bootstrap";

export function Pagination ({
    activePage = 1,
    setActivePage,
    numberOfPages = 1
}) {
    return (
        <BsPagination>
            <BsPagination.First />
            <BsPagination.Prev />

            { buildPageItemsArray() }

            <BsPagination.Next />
            <BsPagination.Last />
        </BsPagination>
    );

    function buildPageItemsArray () {
        var pageItems = [];

        for (let i = 0; i < numberOfPages; i++) {
            pageItems.push(
                <BsPagination.Item
                    // TODO
                    // eslint-disable-next-line
                    active={ activePage == i + 1 }
                    onClick={function () {
                        setActivePage(i + 1);
                    }}
                    key={i}
                >
                    {i + 1}
                </BsPagination.Item>
            );
        }

        return pageItems;
    }
}
