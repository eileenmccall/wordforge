export function getClickHandler (value, fn) {
    return function clickHandler () {
        fn(value);
    };
}

export function getSelectHandler (fn) {
    return function selectHandler (event) {
        return fn(event.target.value);
    };
}

export function checkAnswer (answersArray, answer) {
    return answersArray.some(
        function compareAnswers (acceptableAnswer) {
            return acceptableAnswer.toLowerCase() == answer.toLowerCase();
        }
    );
}