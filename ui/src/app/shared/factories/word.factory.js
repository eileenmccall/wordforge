export function wordFactory ({
    root,
    translation,
    partOfSpeech,
    numberOfExercises,
    numberOfSentences,
    reviewPercentage
}) {
    return {
        root: root || "",
        translation: translation || "",
        partOfSpeech: partOfSpeech || null,
        numberOfExercises,
        numberOfSentences,
        reviewPercentage
    };
}