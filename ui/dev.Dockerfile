FROM node:12.10.0

WORKDIR /usr/app

COPY ./ui/package*.json ./

RUN npm ci -qy

RUN npm install @getify/eslint-plugin-proper-arrows @getify/eslint-plugin-proper-ternary eslint-config-react-app eslint-plugin-flowtype eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-react-hooks

COPY .eslintrc.json .eslintignore  ./

COPY ./ui .

CMD ["npm", "start"]